"""
"""
import numpy as np
import pytest

from os import path

import gwinc
import time

from gwinc.noise import quantum2 as q_new
from gwinc.noise import quantum as q_old


import pylab as pyl

def test_phases(pprint, tpath_join, fpath_join):
    F_Hz = np.logspace(1.2, 3.5, 1000)

    fig = pyl.figure()
    fig.set_size_inches(6, 8)
    ax0 = fig.add_subplot(4, 1, 1)
    ax1 = fig.add_subplot(4, 1, 2)
    ax2 = fig.add_subplot(4, 1, 3)
    ax3 = fig.add_subplot(4, 1, 4)

    for p in [
            gwinc.Struct(
                Tunephase_deg = -2,
                LO_deg = 0,
            ),
            gwinc.Struct(
                Tunephase_deg = 0,
                LO_deg = 0,
            ),
            gwinc.Struct(
                Tunephase_deg = 2,
                LO_deg = 0,
            ),
            gwinc.Struct(
                Tunephase_deg = -2,
                LO_deg = -13,
            ),
    ]:
        budget = gwinc.load_budget('Aplus', freq = F_Hz, )
        del budget.ifo.Laser.Power
        budget.ifo.Laser.ArmPower = 200e3
        budget.ifo.Squeezer.Type = 'Freq Independent'
        budget.ifo.Optics.SRM.Tunephase = p.Tunephase_deg / 180 * np.pi
        budget.ifo.Squeezer.AmplitudedB = 10
        budget.ifo.Squeezer.SQZAngle = -20 / 180 * np.pi
        del budget.ifo.Optics.Quadrature.dc
        budget.ifo.Squeezer.Readout = gwinc.Struct()
        budget.ifo.Squeezer.Readout.Angle = np.pi/2 + p.LO_deg/180 * np.pi
        budget.ifo.Squeezer.Readout.Type = 'Homodyne'
        budget.ifo.Squeezer.Readout.follow_fringe = True
        #budget.ifo.Squeezer.Readout.follow_fringe = False
        sr = q_new.shotrad_debug(freq = F_Hz, ifo = budget.ifo, debug = True)

        sr.theta_IFO = np.unwrap(sr.theta_IFO * 2) / 2
        sr.IFOangle_rad_approx = np.unwrap(sr.IFOangle_rad_approx * 2) / 2
        theta = sr.theta_IFO - sr.theta_IFO[-1]
        print(budget.ifo.Squeezer.Readout.Angle, sr.theta_IFO[-1])
        theta2 = sr.IFOangle_rad_approx - sr.IFOangle_rad_approx[-1]
        line, = ax0.semilogx(F_Hz, theta / np.pi * 180)
        ax0.semilogx(F_Hz, theta2 / np.pi * 180, ls = '--', color = line.get_color())
        ax1.loglog(F_Hz, sr.Gamma_IFO**0.5)
        ax2.loglog(F_Hz, sr.etaS_full)
        line, = ax3.semilogx(F_Hz, sr.Xi_IFO)
        ax3.semilogx(F_Hz, sr.Xi_calc, ls = '--', color = line.get_color())

    ax0.axhline(90, ls = '--')
    #ax.set_ylim(1e-2, 10)
    #ax.set_xlim(5, 200)
    ax0.grid(b=True)
    ax1.grid(b=True)
    fig.tight_layout()
    fig.savefig(tpath_join('SRCL.pdf'), )

    print(sr.HD_angle_rad - np.pi/2)
    print(-sr.ifo.Optics.SRM.Tunephase/2 / -sr.k)
    print("HD diff ", sr.LOdiffRad, sr.ifo.Optics.SRM.Tunephase / sr.LOdiffRad)

    t = .01
    r = (1 - .325)**0.5
    print(t / (np.exp(1j * t / 2) * (1 + r) / (1 + r*np.exp(1j * t))).imag)
    print(4 / .325)

def test_theta_Xi(pprint, tpath_join, fpath_join):
    from gwinc.noise.quantum2 import thetaXietaGamma
    from gwinc.noise.quantum_lib import mats_planewave, adjoint
    LO = mats_planewave.LO(0)
    theta_test = .2
    LOa = adjoint(mats_planewave.Mrotation(theta_test) @ LO)
    theta, Xi, etaGamma = thetaXietaGamma(LOa)
    print(theta)

    LOa = adjoint(LO)
    theta, Xi, etaGamma = thetaXietaGamma(LOa @ mats_planewave.Mrotation(theta_test))
    print(theta)

    theta, Xi, etaGamma = thetaXietaGamma(LOa @ mats_planewave.Mrotation(theta_test) @ mats_planewave.RPNK(1))
    print(etaGamma)



