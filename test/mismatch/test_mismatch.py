"""
"""
import numpy as np
import pytest
import gwinc
# from gwinc.noise import quantum2 as quantum
from gwinc.noise import quantum3 as quantum
from gwinc.struct import Struct
# from gwinc.ifo.noises import Strain
from gwinc.ifo.noises import ifo_power
from gwinc.suspension import precomp_suspension
from gwinc import const
from gwinc import plant
import matplotlib.pyplot as plt
import subprocess
import os
import matplotlib.gridspec as gridspec
from matplotlib.ticker import FormatStrFormatter, FuncFormatter


SEC_GOUY_ARRS = [
    # (np.hstack(([0], np.arange(170, 181))) * np.pi/180, 0),
    # (np.hstack(([0], np.linspace(179, 180, 9))) * np.pi/180, 0),
    (np.arange(170, 181) * np.pi/180, 0, 'CE.yaml'),
    (np.linspace(179, 180, 9) * np.pi/180, 0, 'CE.yaml'),
    (np.arange(0, 11) * np.pi/180, 0, 'CE.yaml'),
    (np.arange(170, 181) * np.pi/180, np.pi/2, 'CE.yaml'),
    (np.arange(0, 11) * np.pi/180, np.pi/2, 'CE.yaml'),
    (np.arange(170, 181) * np.pi/180, np.pi/3, 'CE.yaml'),
    (np.arange(0, 181, 15) * np.pi/180, 0, 'CE.yaml'),
    (np.arange(0, 181, 15) * np.pi/180, 0, 'Aplus.yaml'),
]


ifo_names = [
    # 'CE_mismatch.yaml',
    'CE_ARM_SEC_Gouy180_Phase0.yaml',
    'CE_ARM_SEC_Gouy170_Phase0.yaml',
    'CE_ARM_SEC_Gouy0_Phase0.yaml',
    'CE_ARM_SEC_Gouy180_Phase0_ArmGouy.yaml',
    'CE_ARM_SEC_Gouy170_Phase0_ArmGouy.yaml',
    'CE_ARM_SEC_Gouy170_Phase0_ArmGouy_L0p4.yaml',
    'CE_ARM_SEC_Gouy170_Phase0_g0p6.yaml',
    'CE_ARM_SEC_Gouy180_Phase90.yaml',
    'CE_ARM_SEC_Gouy180_Phase45.yaml',
    'CE_ARM_SEC_Gouy15_Phase0.yaml',
    'CE.yaml',
    'CE20_ARM_SEC_Gouy170_Phase0.yaml',
    'CE20_ARM_SEC_Gouy170_Phase0_g0p6.yaml',
    'CE20.yaml',
    'Aplus_ARM_SEC_Gouy180_Phase0.yaml',
    'Aplus_ARM_SEC_Gouy170_Phase0.yaml',
    'Aplus.yaml',
]


@pytest.mark.parametrize('phases', SEC_GOUY_ARRS)
def test_mismatch_budgets(phases, tpath_join):
    F_Hz = np.logspace(0, 4, 1000)
    budget = gwinc.load_budget(phases[-1])
    # q_budget = quantum.Quantum(ifo=budget.ifo, freq=F_Hz)
    # # q_budget.calibrations.append(Strain)
    # traces = q_budget.run()
    phase = phases[1]
    # gouy_phases = np.array([178, 179, 180]) * np.pi/180
    fnames = []
    for gi, gouy in enumerate(phases[0]):
        traces = compute_noise(budget.ifo, F_Hz, gouy, ARM_SEC=[0.02, phase])
        fig = traces.plot()
        fig.gca().set_ylim(1e-23, 1e-18)
        fig.gca().set_title(
            r'$\phi_\mathrm{{G}}={:0.2f}^\circ \quad \psi_\mathrm{{s}}={:0.2f}^\circ$'.format(gouy*180/np.pi, phase*180/np.pi))
        fname = tpath_join('{:d}.pdf'.format(gi))
        fnames.append(fname)
        fig.savefig(fname)
    cmd = ['pdftk']
    cmd.extend(fnames)
    cmd.extend(['cat', 'output', tpath_join('budgets.pdf')])
    out = subprocess.call(cmd)
    if out == 0:
        for fname in fnames:
            os.remove(fname)


@pytest.mark.parametrize('ifo_name', ifo_names)
def test_transfer_matrix(ifo_name, tpath_join):
    detailed = True
    F_Hz = np.logspace(0, 4, 1000)
    ifo = compute_detailed_noise(F_Hz, ifo_name)

    tmats = [
        ifo.sec.reflARM,
        ifo.sec.clARM,
        ifo.access.transARM,
        ifo.sec.reflSRC,
        ifo.sec.clSRC,
        ifo.Tmats['lossSEC'],
        ifo.access.transETM,
        ifo.fc.REFL,
        ifo.fc.CL,
        ifo.Hmats['AS'],
        ifo.Tmats['FCtrans0'],
    ]

    titles = [
        r'Arm Reflection $r_\mathrm{A}$',
        r'Arm CLTF $F_\mathrm{A}$',
        r'Arm Transmission $t_\mathrm{A}$',
        r'SEC Reflectivity $r_\mathrm{S}$',
        r'SEC CLTF $F_\mathrm{S}$',
        r'SEC Transmission $t_\mathrm{S}$',
        r'Arm Transmission through SEC $t_\mathrm{S}t_\mathrm{A}$',
        r'FC Reflection $r_\mathrm{F}$',
        r'FC CLTF $F_\mathrm{F}$',
        r'AS Port Reflection',
        r'AS Port Transmission',
    ]
    # titles = [title + '\t' for title in titles]

    fnames = [
        'arm_reflection',
        'arm_cltf',
        'arm_transmission',
        'sec_reflection',
        'sec_cltf',
        'sec_transmission',
        'arm_sec_transmission',
        'fc_reflection',
        'fc_cltf',
        'as_reflection',
        'as_transmission',
    ]

    snames = []

    # figs = plot_tf_homs(ifo.sec.reflARM, F_Hz, detailed=detailed, ifo=ifo,
    #                     title=r'Arm Reflection      $r_\mathrm{A}$')
    # # save_transfer_matrix(figs, tpath_join(ifo_name))
    # save_transfer_matrix(figs, tpath_join('arm_reflection'))
    # figs = plot_tf_homs(ifo.sec.clARM, F_Hz, detailed=detailed, ifo=ifo,
    #                     title=r'ARM CLTF      $F_\mathrm{A}$')
    # save_transfer_matrix(figs, tpath_join('arm_cltf'))

    for tmat, title, fname in zip(tmats, titles, fnames):
        figs = plot_tf_homs(tmat, F_Hz, detailed=detailed, ifo=ifo, title=title)
        sname = tpath_join(fname)
        save_transfer_matrix(figs, sname)
        snames.append(sname + '.pdf')

    cmd = ['pdftk']
    cmd.extend(snames)
    cmd.extend(['cat', 'output', tpath_join('transfer_matrix.pdf')])
    subprocess.call(cmd)

    fig = ifo.traces.plot()
    fig.gca().set_ylim(1e-27, 5e-23)
    plot_frequencies(fig.gca(), ifo, plot_tms=True, lw=2)
    fig.savefig(tpath_join('budget.pdf'))
    fig_rg = ifo.relgamma.plot()
    fig_rg.savefig(tpath_join('relgamma.pdf'))

    fig = plotTF(F_Hz, ifo.retD['d_sense'])
    fig.axes[0].set_title('Sensing Function')
    fig.savefig(tpath_join('sensing_function.pdf'))


def test_plant_tfs(tpath_join, fpath_join):
    F_Hz = np.logspace(0, 4, 1000)
    ifo_name = fpath_join('CE_ARM_SEC_Gouy170_Phase0.yaml')
    budget = gwinc.load_budget(ifo_name, freq=F_Hz)
    quant_tfs, plant_tfs = compute_tfs(F_Hz, budget.ifo)

    tmats = [
        'reflARM',
        'clARM',
        'transARM',
        'reflSRC',
        'clSRC',
        'transSEC',
        'transETM',
        'fcREFL',
        'fcCL',
        'HmatAS',
        'TmatFC',
    ]

    titles = [
        r'Arm Reflection $r_\mathrm{A}$',
        r'Arm CLTF $F_\mathrm{A}$',
        r'Arm Transmission $t_\mathrm{A}$',
        r'SEC Reflectivity $r_\mathrm{S}$',
        r'SEC CLTF $F_\mathrm{S}$',
        r'SEC Transmission $t_\mathrm{S}$',
        r'Arm Transmission through SEC $t_\mathrm{S}t_\mathrm{A}$',
        r'FC Reflection $r_\mathrm{F}$',
        r'FC CLTF $F_\mathrm{F}$',
        r'AS Port Reflection',
        r'AS Port Transmission',
    ]

    fnames = [
        'arm_reflection',
        'arm_cltf',
        'arm_transmission',
        'sec_reflection',
        'sec_cltf',
        'sec_transmission',
        'arm_sec_transmission',
        'fc_reflection',
        'fc_cltf',
        'as_reflection',
        'as_transmission',
    ]

    for tmat, title, fname in zip(tmats, titles, fnames):
        figs_quant = plot_tf_homs(
            quant_tfs[tmat], F_Hz, detailed=True, ifo=quant_tfs, plot_freqs=False)
        sname_quant = tpath_join(fname + '_quant')
        save_transfer_matrix(figs_quant, sname_quant)

        figs_plant = plot_tf_homs(
            plant_tfs[tmat], F_Hz, detailed=True, ifo=plant_tfs, plot_freqs=False)
        sname_plant = tpath_join(fname + '_plant')
        save_transfer_matrix(figs_plant, sname_plant)


def compute_noise(
        ifo,
        F_Hz,
        SRC_gouy_rad,
        ARM_SEC=[0, 0],
        IFO_OMC=[0, 0],
        SQZ_OMC=[0, 0],
        FC_SQZ=[0, 0],
):

    ifo.Optics.SRM.SRCGouy_rad     = SRC_gouy_rad
    ifo.Optics.MM_ARM_SRC            = ARM_SEC[0]
    ifo.Optics.MM_ARM_SRCphi         = ARM_SEC[1]
    ifo.Optics.MM_IFO_OMC            = IFO_OMC[0]
    ifo.Optics.MM_IFO_OMCphi         = IFO_OMC[1]
    ifo.Squeezer.MM_SQZ_OMC          = SQZ_OMC[0]
    ifo.Squeezer.MM_SQZ_OMCphi       = SQZ_OMC[1]
    ifo.Squeezer.FilterCavity.L_mm   = FC_SQZ[0]
    ifo.Squeezer.FilterCavity.psi_mm = FC_SQZ[1]
    budget = quantum.Quantum(ifo=ifo, freq=F_Hz)
    traces = budget.run()
    return traces


def get_shotrad(F_Hz, ifo):
    sustf = precomp_suspension(F_Hz, ifo)
    power = ifo_power(ifo)
    retD = quantum.shotrad(F_Hz, ifo, sustf, power)
    return retD


def compute_detailed_noise(F_Hz, ifo_name):
    q_budget  = gwinc.load_budget(ifo_name, freq=F_Hz)
    # q_budget  = gwinc.load_budget(ifo_name, freq=F_Hz,
    #                               bname='Quantum')
    rg_budget = gwinc.load_budget(ifo_name, freq=F_Hz,
                                  bname='QuantumRelGamma')
    q_traces  = q_budget.run()
    rg_traces = rg_budget.run()
    retD = get_shotrad(F_Hz, q_budget.ifo)

    La = q_budget.ifo.Infrastructure.Length
    Ti = q_budget.ifo.Optics.ITM.Transmittance
    Ts = q_budget.ifo.Optics.SRM.Transmittance
    Ls = q_budget.ifo.Optics.SRM.CavityLength
    ua = 1 - np.sqrt(1 - Ti)
    us = 1 - np.sqrt(1 - Ts)
    rs = np.sqrt(1 - Ts)

    fa = const.c*ua/(4*np.pi*La)
    fp = const.c*ua/(4*np.pi*La) * (2 - us)/us
    fs = const.c*us/(4*np.pi*Ls)
    fsr = const.c/(2*La)
    fp2 = fa * (1 + rs) / (1 - rs)
    dfs = const.c / (4*np.pi) * np.sqrt(Ti/(La*Ls))

    Ri = q_budget.ifo.Optics.Curvature.ETM
    Re = q_budget.ifo.Optics.Curvature.ITM
    gi = 1 - La/Ri
    ge = 1 - La/Re
    tms = np.arccos(np.sign(gi) * np.sqrt(gi*ge)) / np.pi * fsr

    return Struct(
        ifo=q_budget.ifo,
        traces=q_traces.Quantum,
        relgamma=rg_traces,
        Hmats=retD['mats'].H,
        Tmats=retD['mats'].T,
        mats=retD['mats'],
        access=retD['access'],
        sec=retD['access'].sec,
        fc=retD['access'].FC0.fc,
        retD=retD,
        fa=fa,
        fp=fp,
        fs=fs,
        fsr=fsr,
        fp2=fp2,
        dfs=dfs,
        tms=tms,
    )


def compute_tfs(F_Hz, ifo):
    sustf = precomp_suspension(F_Hz, ifo)
    power = ifo_power(ifo)
    retD_quant = quantum.shotrad(F_Hz, ifo, sustf, power)
    mats_plant, access_plant = plant.DRFPMI(F_Hz, ifo, sustf, power)

    quant_tfs = Struct(
        reflARM=retD_quant['access'].sec.reflARM,
        clARM=retD_quant['access'].sec.clARM,
        transARM=retD_quant['access'].transARM,
        reflSRC=retD_quant['access'].sec.reflSRC,
        clSRC=retD_quant['access'].sec.clSRC,
        transSEC=retD_quant['mats'].T['lossSEC'],
        transETM=retD_quant['access'].transETM,
        fcREFL=retD_quant['access'].FC0.fc.REFL,
        fcCL=retD_quant['access'].FC0.fc.CL,
        HmatAS=retD_quant['mats'].H['AS'],
        TmatFC=retD_quant['mats'].T['FCtrans0'],
    )

    plant_tfs = Struct(
        reflARM=access_plant.arm.reflARM,
        clARM=access_plant.arm.clARM,
        transARM=access_plant.arm.transARM,
        reflSRC=access_plant.sec.reflSRC,
        clSRC=access_plant.sec.clSRC,
        transSEC=mats_plant.T['lossSEC'],
        transETM=access_plant.transETM,
        fcREFL=access_plant.FC0.fc.REFL,
        fcCL=access_plant.FC0.fc.CL,
        HmatAS=mats_plant.H['AS'],
        TmatFC=mats_plant.T['FCtrans0'],
    )

    return quant_tfs, plant_tfs


def plot_tf(tf_mats, F_Hz, is_to_hom=False, is_from_hom=False, fig=None, ifo=None,
            plot_freqs=True):
    # qind = 2*is_hom
    # pind = 1 + 2*is_hom

    q_to = 0 + 2*is_to_hom
    p_to = 1 + 2*is_to_hom
    q_fr = 0 + 2*is_from_hom
    p_fr = 1 + 2*is_from_hom

    if fig is None:
        fig = plotTF(F_Hz, tf_mats[:, p_to, q_fr], label='pq')
    else:
        plotTF(F_Hz, tf_mats[:, p_to, q_fr], label='pq')
    plotTF(F_Hz, tf_mats[:, q_to, q_fr], *fig.axes, label='qq')
    plotTF(F_Hz, tf_mats[:, p_to, p_fr], *fig.axes, ls='--', label='pp')
    plotTF(F_Hz, tf_mats[:, q_to, p_fr], *fig.axes, ls='-.', label='qp')
    # phase2amp0 = np.allclose(tf_mats[:, q_to, p_fr], 0)
    # if not phase2amp0:
    #     plotTF(F_Hz, tf_mats[:, q_to, p_fr], *fig.axes, ls='-.', label='qp')
    # print("qp zero? ", phase2amp0)

    if plot_freqs:
        if ifo:
            for ax in fig.axes:
                # ax.axvline(ifo.fa, ls=':', c='xkcd:slate', label=r'$f_\mathrm{a}$')
                # ax.axvline(ifo.fp, ls=':', c='xkcd:emerald green', label=r'$f_\mathrm{p}$')
                # ax.axvline(ifo.fsr, ls=':', c='xkcd:sienna', label=r'$f_\mathrm{fsr}$')
                plot_frequencies(ax, ifo)
    handles, labels = fig.axes[0].get_legend_handles_labels()
    fig.axes[0].legend(handles[:4], labels[:4])
    fig.axes[1].legend(handles[4:], labels[4:])

    return fig


def plot_frequencies(ax, ifo, plot_tms=False, **kwargs):
    ax.axvline(ifo.fa, ls=':', c='xkcd:slate', label=r'$f_\mathrm{a}$', **kwargs)
    ax.axvline(ifo.fp, ls=':', c='xkcd:emerald green', label=r'$f_\mathrm{p}$', **kwargs)
    ax.axvline(ifo.fsr, ls=':', c='xkcd:sienna', label=r'$f_\mathrm{fsr}$', **kwargs)
    if plot_tms:
        for nn in np.arange(4):
            ax.axvline(np.abs(2*ifo.tms - nn*ifo.fsr), ls=':', c='xkcd:bright blue', **kwargs)


def plot_tf_homs(tf_mats, F_Hz, detailed=False, ifo=None, title='', plot_freqs=True):
    fig0 = plot_tf(
        tf_mats, F_Hz, is_to_hom=False, is_from_hom=False, ifo=ifo, plot_freqs=plot_freqs)
    fig0.axes[0].set_title(title + ' Fundamental')
    out = [fig0]
    if tf_mats.shape[1] > 2:
        # fig0.axes[0].set_title('Fundamental')
        fig1 = plot_tf(
            tf_mats, F_Hz, is_to_hom=True, is_from_hom=True, ifo=ifo, plot_freqs=plot_freqs)
        fig1.axes[0].set_title(title + ' HOM')
        out.append(fig1)
        if detailed:
            fig2 = plot_tf(
                tf_mats, F_Hz, is_to_hom=False, is_from_hom=True, ifo=ifo, plot_freqs=plot_freqs)
            fig2.axes[0].set_title(title + ' HOM to Fundamental')
            fig3 = plot_tf(
                tf_mats, F_Hz, is_to_hom=True, is_from_hom=False, ifo=ifo, plot_freqs=plot_freqs)
            fig3.axes[0].set_title(title + ' Fundamental to HOM')
            out.append(fig2)
            out.append(fig3)
    return out


def save_transfer_matrix(figs, fig_name):
    if len(figs) > 1:
        fignames = ['fun', 'hom', 'hom_fun', 'fun_hom']
        fnames = [
            '{:s}_{:s}.pdf'.format(fig_name, figname)
            for figname in fignames]
        figs = [figs[0], figs[2], figs[3], figs[1]]
        fnames = [fnames[0], fnames[2], fnames[3], fnames[1]]
        for fig, fname in zip(figs, fnames):
            print('saving', fname)
            fig.savefig(fname)

        cmd1 = ['pdfjam']
        cmd1.extend(fnames)
        cmd1.extend(['--nup', '2x2', '--outfile', fig_name + '_large.pdf'])
        cmd2 = ['pdfcrop', '--margins', '10', fig_name + '_large.pdf', fig_name + '.pdf']
        print('jaming')
        subprocess.call(cmd1)
        print('cropping')
        subprocess.call(cmd2)
        # del_names = ['fun.pdf', 'hom_fun.pdf', 'fun_hom.pdf', 'hom.pdf', 'matrix_large.pdf']
        print('deleting')
        fnames.append(fig_name + '_large.pdf')
        for fname in fnames:
            try:
                os.remove(fname)
            except FileNotFoundError:
                pass
    else:
        figs[0].savefig(fig_name + '.pdf')

    plt.close('all')


def plotTF(ff, tf, mag_ax=None, phase_ax=None, **kwargs):
    if not(mag_ax and phase_ax):
        if (mag_ax is not None) or (phase_ax is not None):
            msg = 'If one of the phase or magnitude axes is given,'
            msg += ' the other must be given as well.'
            raise ValueError(msg)
        newFig = True
    else:
        newFig = False

    if newFig:
        fig = plt.figure()
        gs = gridspec.GridSpec(2, 1, height_ratios=[1, 1], hspace=0.05)
        mag_ax = fig.add_subplot(gs[0])
        phase_ax = fig.add_subplot(gs[1], sharex=mag_ax)
    else:
        old_ylims = mag_ax.get_ylim()

    mag_ax.loglog(ff, np.abs(tf), **kwargs)
    mag_ax.set_ylabel('Magnitude')
    magTF = np.abs(tf)
    # If the TF is close to being constant magnitude, increase ylims
    # in order to show y tick labels and avoid a misleading plot.
    if np.abs(np.max(magTF)/np.min(magTF)) < 10:
        # mag_ax.set_yscale('linear')
        mag_ax.set_ylim(np.mean(magTF)/10.1, np.mean(magTF)*10.1)

    # If plotting ontop of an old TF, adjust the ylims so that the old TF
    # is still visible
    if not newFig:
        new_ylims = mag_ax.get_ylim()
        mag_ax.set_ylim(min(old_ylims[0], new_ylims[0]),
                        max(old_ylims[1], new_ylims[1]))

    mag_ax.set_xlim(min(ff), max(ff))
    phase_ax.set_ylim(-185, 185)
    # ticks = np.linspace(-180, 180, 7)
    ticks = np.arange(-180, 181, 45)
    phase_ax.yaxis.set_ticks(ticks)
    phase_ax.semilogx(ff, np.angle(tf, True), **kwargs)
    phase_ax.set_ylabel('Phase [deg]')
    phase_ax.set_xlabel('Frequency [Hz]')
    plt.setp(mag_ax.get_xticklabels(), visible=False)
    mag_ax.grid(True, which='both', alpha=0.5)
    mag_ax.grid(True, alpha=0.25, which='minor')
    phase_ax.grid(True, which='both', alpha=0.5)
    phase_ax.grid(True, alpha=0.25, which='minor')
    if newFig:
        return fig
