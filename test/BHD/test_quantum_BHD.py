"""
"""
import numpy as np
import pytest

from os import path

import gwinc
import time

from gwinc.noise import quantum2 as q_new
from gwinc.noise import quantum as q_old
import inspiral_range


import pylab as pyl
from matplotlib import gridspec


RANGE_PARAMS_NS = dict(m1=1.4, m2=1.4)
RANGE_PARAMS_BH = dict(m1=70, m2=70)

def test_Aplus_LO(pprint, tpath_join, fpath_join):
    F_Hz = np.logspace(1, 3.5, 3000)

    budget_nom = gwinc.load_budget(fpath_join('Aplus.yaml'), freq = F_Hz)
    traces_nom = budget_nom.run()
    fig_all = pyl.figure()
    fig_all.set_size_inches(10, 6)
    gs = fig_all.add_gridspec(2, 2)
    ax_all = fig_all.add_subplot(gs[0, :])
    ax_allrel = fig_all.add_subplot(gs[1, 0])
    ax_allrelQ = fig_all.add_subplot(gs[1, 1])

    def plot_ifos(ifos):
        for idx, (iname, kw1, kw2) in enumerate(ifos):
            if path.splitext(iname)[1]:
                budgetQRG = gwinc.load_budget(fpath_join(iname), freq = F_Hz, bname = 'QuantumRelGamma')
                budget = gwinc.load_budget(fpath_join(iname), freq = F_Hz)
            else:
                budgetQRG = gwinc.load_budget(iname, freq = F_Hz, bname = 'QuantumRelGamma')
                budget = gwinc.load_budget(iname, freq = F_Hz)

            ax_h = fig.add_subplot(len(ifos), 2, 2*idx+1)
            ax_QRG = fig.add_subplot(len(ifos), 2, 2*idx+2)
            tracesQRG = budgetQRG.run()
            traces = budget.run()

            def range_data(PARAMS):
                metrics, H = inspiral_range.all_ranges(traces.freq, traces.psd, **PARAMS)
                range_func = 'range'
                subtitle = 'inspiral {func} {m1}/{m2} $\mathrm{{M}}_\odot$: {fom:.0f} {unit}'.format(
                    func=range_func,
                    m1=H.params['m1'],
                    m2=H.params['m2'],
                    fom=metrics[range_func][0],
                    unit=metrics[range_func][1] or '',
                )
                return metrics[range_func][0], subtitle
            range_NS = range_data(RANGE_PARAMS_NS)
            range_BH = range_data(RANGE_PARAMS_BH)

            traces.plot(ax = ax_h)
            ax_h.set_title(kw1['label'] + ':   ' + range_NS[1])
            ax_QRG.set_title(range_BH[1])
            tracesQRG.plot(ax = ax_QRG)
            label = kw2.pop('label') + ' {:.0f}Mpc NS1.4, {:.2f}Gpc BH70'.format(range_NS[0], range_BH[0]/1e3)
            kw2['label'] = label
            ax_all.loglog(traces.freq, traces.asd, **kw2)
            ax_allrel.loglog(traces.freq, traces.asd / traces_nom.asd, **kw2)
            ax_allrelQ.loglog(traces.freq, traces['Quantum'].asd / traces_nom['Quantum'].asd, **kw2)

    ifos = [
        ("Aplusn1rad.yaml", dict(label = 'LO -0.1rad'), dict(label = 'LO -0.1rad')),
        ("Aplusn2rad.yaml", dict(label = 'LO -0.2rad'), dict(label = 'LO -0.2rad')),
        ("Aplusn3rad.yaml", dict(label = 'LO -0.3rad'), dict(label = 'LO -0.3rad')),
        ("Aplusn4rad.yaml", dict(label = 'LO -0.4rad'), dict(label = 'LO -0.4rad')),
    ]
    fig = pyl.figure()
    fig.set_size_inches(12, 4 * len(ifos))
    plot_ifos(ifos)
    fig.tight_layout()
    fig.savefig(tpath_join('budgetQRG_neg.pdf'))

    ifos = [
        ("Aplusp1rad.yaml", dict(label = 'LO +0.1rad'), dict(label = 'LO +0.1rad')),
        ("Aplusp2rad.yaml", dict(label = 'LO +0.2rad'), dict(label = 'LO +0.2rad')),
        ("Aplusp3rad.yaml", dict(label = 'LO +0.3rad'), dict(label = 'LO +0.3rad')),
    ]
    fig = pyl.figure()
    fig.set_size_inches(12, 4 * len(ifos))
    plot_ifos(ifos)
    fig.tight_layout()
    fig.savefig(tpath_join('budgetQRG_pos.pdf'))

    ax_all.set_xlim(10, 3e3)
    ax_all.set_ylim(1e-24, 3e-23)
    ax_all.grid(b=True, which = 'both')
    ax_all.legend(loc = 'upper right', ncol = 2)
    ax_allrel.set_xlim(10, 1e3)
    ax_allrel.grid(b=True, which = 'both')
    ax_all.set_xlabel('Frequency [Hz]')
    ax_allrel.set_xlabel('Frequency [Hz]')
    ax_allrelQ.set_xlabel('Frequency [Hz]')
    ax_allrelQ.set_xlim(10, 1e3)
    ax_allrelQ.grid(b=True, which = 'both')
    ax_all.set_ylabel('Strain ASD h/rtHz')
    ax_allrel.set_ylabel('Total ASD relative to 0rad LO')
    ax_allrelQ.set_ylabel('Quantum ASD relative to 0rad LO')
    ax_all.set_title('GWINC Strain ASD at varying LO angles')
    ax_allrel.set_title('Relative GWINC Strain ASD')
    ax_allrelQ.set_title('Relative Quantum Strain ASD')
    fig_all.tight_layout()
    fig_all.savefig(tpath_join('compare.pdf'))


def test_Aplus_scan(pprint, tpath_join, fpath_join):
    F_Hz = np.logspace(1, 3, 500)

    budget = gwinc.load_budget(fpath_join('Aplus.yaml'), freq = F_Hz)
    fdetune = np.linspace(-60, -40, 10)
    SQZang = np.linspace(0, -0.4, 10)

    fd_m, SQZ_m = np.meshgrid(fdetune, SQZang)

    with np.nditer(
        [fd_m, SQZ_m, None, None], [],
        [['readonly'], ['readonly'], ['writeonly', 'allocate'], ['writeonly', 'allocate']]
    ) as it:
        for (fd, ang, outNS, outBH) in it:
            pprint(ang, fd, it.iterindex / it.itersize)
            budget.ifo.Optics.Quadrature.dc = float(np.pi/2 + ang)
            budget.ifo.Squeezer.SQZAngle = float(ang)
            budget.ifo.Squeezer.FilterCavity.fdetune = float(fd)
            traces = budget.run()
            psd_full = traces.psd
            #pprint(psd_full)
            #fig = pyl.figure()
            #ax = fig.add_subplot(1,1,1)
            #ax.loglog(traces.freq, psd_full)
            #fig.tight_layout()
            #fig.savefig(tpath_join('test.pdf'))
            R = inspiral_range.range(traces.freq, psd_full, **RANGE_PARAMS_NS)
            pprint('Range:', R)
            outNS[()] = R
            R = inspiral_range.range(traces.freq, psd_full, **RANGE_PARAMS_BH)
            pprint('Range:', R)
            outBH[()] = R
        contourNS = it.operands[2]
        pprint(contourNS)
        contourBH = it.operands[3]
        pprint(contourBH)

    import h5py
    with h5py.File(fpath_join('contour.h5'), 'w') as hdf5:
        hdf5['rangeNS'] = contourNS
        hdf5['rangeBH'] = contourBH
        hdf5['fd_m'] = fd_m
        hdf5['SQZ_m'] = SQZ_m
        hdf5['fd'] = fdetune
        hdf5['SQZ'] = SQZang
    return

def test_Aplus_plot(pprint, tpath_join, fpath_join):
    import h5py
    with h5py.File(fpath_join('contour.h5'), 'r') as hdf5:
        fig = pyl.figure()
        ax = fig.add_subplot(1, 1, 1)
        extent = (
            hdf5['fd'][0],
            hdf5['fd'][-1],
            hdf5['SQZ'][-1],
            hdf5['SQZ'][0],
        )
        rel = hdf5['rangeNS'] / np.max(hdf5['rangeNS'])
        pm = ax.imshow(
            rel,
            cmap = 'copper',
            extent = extent,
        )

        CS = ax.contour(
            hdf5['fd'], hdf5['SQZ'], rel,
            levels = [.80, .9, .95, .99],
            colors = ['black', 'blue', 'purple', 'red'],
            #colors = [],
            alpha = .7,
            linewidths = 1,
        )
        ax.clabel(CS, inline=1, fontsize=8)
        ax.set_aspect('auto')
        ax.set_ylabel('Homodyne and SQZ angles [rad]')
        ax.set_xlabel('Filter Cavity Detuning [Hz] \n using 1000ppm T_in')
        fig.colorbar(pm, label = 'BNS 1.4/1.4Msol comoving inspiral range (relative max)')
        fig.tight_layout()
        fig.savefig(tpath_join('rangeNS.pdf'))

        fig = pyl.figure()
        ax = fig.add_subplot(1, 1, 1)
        extent = (
            hdf5['fd'][0],
            hdf5['fd'][-1],
            hdf5['SQZ'][-1],
            hdf5['SQZ'][0],
        )
        rel = hdf5['rangeBH'] / np.max(hdf5['rangeBH'])
        pm = ax.imshow(
            rel,
            cmap = 'copper',
            extent = extent,
        )

        CS = ax.contour(
            hdf5['fd'], hdf5['SQZ'], rel,
            levels = [.80, .9, .95, .99],
            colors = ['black', 'blue', 'purple', 'red'],
            #colors = [],
            alpha = .7,
            linewidths = 1,
        )
        ax.clabel(CS, inline=1, fontsize=8)
        ax.set_aspect('auto')
        ax.set_ylabel('Homodyne and SQZ angles [rad]')
        ax.set_xlabel('Filter Cavity Detuning [Hz] \n using 1000ppm T_in')
        fig.colorbar(pm, label = 'BBH 70/70Msol comoving inspiral range (relative max)')
        fig.tight_layout()
        fig.savefig(tpath_join('rangeBH.pdf'))

    return
