"""
"""
import numpy as np
import pytest

from os import path

import gwinc
import time

from gwinc.noise import quantum2 as q_new
from gwinc.noise import quantum as q_old


import pylab as pyl

ifos_cmp = [
    "Aplus",
    'CE2silica',
    "Aplus.yaml",
    'CE2silica.yaml',
]
ifos = [
    "Aplus.yaml",
    "Aplus_lossless.yaml",
    "Aplus_Lrms.yaml",
    "Aplus_B.yaml",
    "Aplus_MMFC.yaml",
    "Aplus_MMFC2.yaml",
    "Aplus_MMFCwGouy.yaml",
    "Aplus_MMIFO.yaml",
    "Aplus_MMIFO2.yaml",
    "Aplus_MMIFO3.yaml",
    "Aplus_MMIFO4.yaml",
    # "Aplus_MMIFOwGouy.yaml", same as Aplus_MMFC
    "Aplus_MMIFO_SRC.yaml",
    "Aplus_MMIFO_SRC2.yaml",
    "Aplus_MMIFO_SRC3.yaml",
]

@pytest.mark.parametrize("budget", ifos)
def test_Aplus_variants(budget, pprint, tpath_join, fpath_join):
    F_Hz = np.logspace(1, 4, 100)

    if path.splitext(budget)[1]:
        budget = gwinc.load_budget(fpath_join(budget), freq = F_Hz)
    else:
        budget = gwinc.load_budget(budget, freq = F_Hz)

    budget_n = q_new.Quantum(ifo = budget.ifo, freq = F_Hz)
    traces_n = budget_n.run()
    fig2 = traces_n.plot()
    fig2.savefig(tpath_join('budget.pdf'))


# @pytest.mark.parametrize("budget", ifos)
@pytest.mark.parametrize("budget", ifos_cmp)
def test_APlus_compare(budget, pprint, tpath_join, fpath_join):
    N = 3
    F_Hz = np.logspace(1, 4, 1000)

    if path.splitext(budget)[1]:
        budget = gwinc.load_budget(fpath_join(budget), freq = F_Hz)
    else:
        budget = gwinc.load_budget(budget, freq = F_Hz)

    budget_o = q_old.Quantum(ifo = budget.ifo, freq = F_Hz)
    budget_n = q_new.Quantum(ifo = budget.ifo, freq = F_Hz)

    start = time.time()
    for idx in range(N):
        #hack, not sure a better way to ensure this is cleared
        budget_o._precomp = {}
        budget_o.run()
    end1 = time.time()

    for idx in range(N):
        #hack, not sure a better way to ensure this is cleared
        budget_n._precomp = {}
        budget_n.run()
    end2 = time.time()

    time_old = (end1 - start)/N
    time_new = (end2 - end1)/N
    pprint("Time Comparison: old {:.2f}ms, new {:.2f}ms, {:0.2f} relative change".format(time_old * 1000, time_new * 1000, time_old / time_new))

    traces_o = budget_o.run()
    fig1 = traces_o.plot()
    fig1.savefig(tpath_join('plot_old.pdf'))

    traces_n = budget_n.run()
    try:
        fig2 = traces_n.plot()
        fig2.savefig(tpath_join('plot_new.pdf'))
    except ValueError:
        pass

    traces2_labelmap = {}
    for trace in traces_n:
        traces2_labelmap[trace.style['label']] = trace

    fig = pyl.figure()
    ax = fig.add_subplot(1, 1, 1)
    for trace1 in traces_o:
        try:
            st = dict(trace1.style)
            st.pop('ls', None)
            trace2 = traces2_labelmap[trace1.style['label']]
            ax.loglog(trace1.freq, trace2.asd / trace1.asd, **st)
        except KeyError:
            pass
    ax.legend(ncol = 2)
    fig.tight_layout()
    fig.savefig(tpath_join('traces_ratio.pdf'.format(trace1.name)), )


@pytest.mark.parametrize("budget", ifos)
def test_Aplus_plots(budget, pprint, tpath_join, fpath_join):
    F_Hz = np.logspace(1, 4, 100)

    if path.splitext(budget)[1]:
        budget = gwinc.load_budget(fpath_join(budget), freq = F_Hz, bname = 'QuantumRelGamma')
    else:
        budget = gwinc.load_budget(budget, freq = F_Hz, bname = 'QuantumRelGamma')

    traces = budget.run()
    fig2 = traces.plot()
    fig2.savefig(tpath_join('budget.pdf'))


def test_Aplus_vary_MM(pprint, tpath_join, fpath_join):
    F_Hz = np.logspace(1, 4.5, 400)

    budget = gwinc.load_budget(fpath_join('Aplus.yaml'), freq = F_Hz, bname = 'QuantumRelGamma')
    del budget.ifo.Laser.Power
    budget.ifo.Laser.ArmPower         = 200e3
    budget.ifo.Optics.MM_IFO_OMC      = 0.00
    budget.ifo.Optics.MM_IFO_OMCphi   = 0
    budget.ifo.Squeezer.MM_SQZ_OMC    = 0.02
    budget.ifo.Squeezer.MM_SQZ_OMCphi = 0.00

    #budget.ifo.Optics.SRM.SRCGouy_rad = -0.785

    fig = pyl.figure()
    ax = fig.add_subplot(1, 1, 1)
    for phi in np.linspace(0, np.pi, 10):
        budget.ifo.Squeezer.MM_SQZ_OMCphi = phi
        traces = budget.run()
        ax.loglog(F_Hz, traces.QuantumMM.psd, label = "phi={:0.2f}".format(phi))
    ax.legend()
    fig.tight_layout()
    ax.set_ylim(1e-2, 1e-1)
    fig.savefig(tpath_join('test_SQZOMC.pdf'))

    budget.ifo.Optics.MM_IFO_OMC      = 0.02
    budget.ifo.Optics.MM_IFO_OMCphi   = 0
    budget.ifo.Squeezer.MM_SQZ_OMC    = 0.00
    budget.ifo.Squeezer.MM_SQZ_OMCphi = 0.00

    #budget.ifo.Optics.SRM.SRCGouy_rad = -0.785

    fig = pyl.figure()
    ax = fig.add_subplot(1, 1, 1)
    for phi in np.linspace(0, np.pi, 10):
        budget.ifo.Optics.MM_IFO_OMCphi   = phi
        traces = budget.run()
        ax.loglog(F_Hz, traces.QuantumMM.psd, label = "phi={:0.2f}".format(phi))
    ax.legend()
    ax.set_ylim(1e-2, 1e-1)
    fig.tight_layout()
    fig.savefig(tpath_join('test_IFOOMC.pdf'))

    budget.ifo.Optics.MM_IFO_OMC      = 0.02
    budget.ifo.Optics.MM_IFO_OMCphi   = 0
    budget.ifo.Squeezer.MM_SQZ_OMC    = 0.02
    budget.ifo.Squeezer.MM_SQZ_OMCphi = 0.00

    #budget.ifo.Optics.SRM.SRCGouy_rad = -0.785

    fig = pyl.figure()
    ax = fig.add_subplot(1, 1, 1)
    for phi in np.linspace(0, np.pi, 10):
        budget.ifo.Squeezer.MM_SQZ_OMCphi = phi
        traces = budget.run()
        ax.loglog(F_Hz, traces.QuantumMM.psd, label = "phi={:0.2f}".format(phi))
    ax.legend()
    ax.set_ylim(1e-2, 1)
    fig.tight_layout()
    fig.savefig(tpath_join('test_DOUBLE.pdf'))

    budget.ifo.Optics.MM_IFO_OMCphi   = 0
    budget.ifo.Squeezer.MM_SQZ_OMCphi = 0.00
    fig = pyl.figure()
    ax = fig.add_subplot(1, 1, 1)
    for phi in np.linspace(0, np.pi, 10):
        budget.ifo.Optics.MM_IFO_OMCphi   = phi
        traces = budget.run()
        ax.loglog(F_Hz, traces.QuantumMM.psd, label = "phi={:0.2f}".format(phi))
    ax.legend()
    ax.set_ylim(1e-2, 1)
    fig.tight_layout()
    fig.savefig(tpath_join('test_DOUBLE2.pdf'))

    budget.ifo.Optics.MM_IFO_OMCphi   = 0
    budget.ifo.Squeezer.MM_SQZ_OMCphi = 0.00
    fig = pyl.figure()
    ax = fig.add_subplot(1, 1, 1)
    for phi in np.linspace(0, np.pi, 10):
        budget.ifo.Squeezer.MM_SQZ_OMCphi = phi
        budget.ifo.Optics.MM_IFO_OMCphi   = phi
        traces = budget.run()
        ax.loglog(F_Hz, traces.QuantumMM.psd, label = "phi={:0.2f}".format(phi))
    ax.legend()
    ax.set_ylim(1e-2, 1)
    fig.tight_layout()
    fig.savefig(tpath_join('test_DOUBLE3.pdf'))

    budget.ifo.Optics.MM_IFO_OMCphi   = 0
    budget.ifo.Squeezer.MM_SQZ_OMCphi = 0.00
    fig = pyl.figure()
    ax = fig.add_subplot(1, 1, 1)
    for phi in np.linspace(0, np.pi, 10):
        budget.ifo.Squeezer.MM_SQZ_OMCphi = phi
        budget.ifo.Optics.MM_IFO_OMCphi   = -phi
        traces = budget.run()
        ax.loglog(F_Hz, traces.QuantumMM.psd, label = "phi={:0.2f}".format(phi))
    ax.legend()
    ax.set_ylim(1e-2, 1)
    fig.tight_layout()
    fig.savefig(tpath_join('test_DOUBLE4.pdf'))

    budget.ifo.Optics.MM_IFO_OMCphi   = 0
    budget.ifo.Squeezer.MM_SQZ_OMCphi = 0.00
    fig = pyl.figure()
    ax = fig.add_subplot(1, 1, 1)
    for phi in np.linspace(0, np.pi, 10):
        budget.ifo.Squeezer.MM_SQZ_OMCphi = phi
        budget.ifo.Optics.MM_IFO_OMCphi   = phi + np.pi
        traces = budget.run()
        ax.loglog(F_Hz, traces.QuantumMM.psd, label = "phi={:0.2f}".format(phi))
    ax.legend()
    ax.set_ylim(1e-2, 1)
    fig.tight_layout()
    fig.savefig(tpath_join('test_DOUBLE5.pdf'))

    budget.ifo.Optics.MM_IFO_OMC      = 0.03
    budget.ifo.Optics.MM_IFO_OMCphi   = 0
    budget.ifo.Squeezer.MM_SQZ_OMC    = 0.01
    budget.ifo.Squeezer.MM_SQZ_OMCphi = 0.00
    fig = pyl.figure()
    ax = fig.add_subplot(1, 1, 1)
    for phi in np.linspace(0, np.pi, 10):
        budget.ifo.Squeezer.MM_SQZ_OMCphi = phi
        budget.ifo.Optics.MM_IFO_OMCphi   = -phi
        traces = budget.run()
        ax.loglog(F_Hz, traces.QuantumMM.psd, label = "phi={:0.2f}".format(phi))
    ax.legend()
    ax.set_ylim(1e-2, 1)
    fig.tight_layout()
    fig.savefig(tpath_join('test_DOUBLEreal.pdf'))
