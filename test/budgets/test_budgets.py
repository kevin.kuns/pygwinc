"""
"""
import numpy as np
import gwinc
from gwinc import load_budget
import gwinc.io as io
from copy import deepcopy
import pytest


@pytest.mark.parametrize("ifo", gwinc.IFOS)
def test_load(ifo, pprint, tpath_join, fpath_join):
    B = load_budget(ifo)
    trace = B.run()
    fig = trace.plot()
    fig.savefig(tpath_join('budget_{}.pdf'.format(ifo)))

@pytest.mark.parametrize("ifo", gwinc.IFOS)
def test_quantum(ifo, pprint, tpath_join, fpath_join):
    B = load_budget(ifo)
    trace = B.run()
    fig = trace['Quantum'].plot()
    fig.savefig(tpath_join('budget_{}.pdf'.format(ifo)))

@pytest.mark.generate
@pytest.mark.parametrize("ifo", gwinc.IFOS)
def test_save_budgets(ifo, fpath_join):
    B = load_budget(ifo)
    traces = B.run()
    io.save_hdf5(traces, fpath_join(ifo + '.h5'))


@pytest.mark.parametrize("ifo", gwinc.IFOS)
def test_check_noise(ifo, fpath_join, compare_noise):
    try:
        ref_traces = io.load_hdf5(fpath_join(ifo + '.h5'))
    except OSError:
        return
    budget = load_budget(ifo, freq=ref_traces.freq)
    traces = budget.run()
    compare_noise(traces, ref_traces)


@pytest.mark.logic
@pytest.mark.fast
def test_update_freq():
    """
    Test three methods of updating a Budget frequency
    """
    freq1 = np.logspace(1, 3, 10)
    freq2 = np.logspace(0.8, 3.5, 11)
    freq3 = np.logspace(0.5, 3.6, 12)
    budget = gwinc.load_budget('Aplus', freq=freq1)
    traces1 = budget.run()
    traces2 = budget.run(freq=freq2)
    budget.freq = freq3
    traces3 = budget.run()
    assert np.all(traces1.freq == freq1)
    assert np.all(traces2.freq == freq2)
    assert np.all(traces3.freq == freq3)


@pytest.mark.logic
@pytest.mark.fast
def test_freq_spec_error():
    """
    Test that three methods of setting Budget frequencies raise errors
    """
    freq = [1, 2, 3]
    with pytest.raises(gwinc.InvalidFrequencySpec):
        budget = gwinc.load_budget('Aplus', freq=freq)
    with pytest.raises(AssertionError):
        budget = gwinc.load_budget('Aplus')
        traces = budget.run(freq=freq)
    with pytest.raises(AssertionError):
        budget = gwinc.load_budget('Aplus')
        budget.freq = freq

