"""
"""
import numpy as np
import pytest
import matplotlib.pyplot as plt


import gwinc
# from gwinc import Struct
from gwinc.noise import quantum2 as q_new
from gwinc.noise import quantum as q_old


ifos = [
    'Aplus_RSE',
    'Aplus_SR',
    'Aplus_20deg_homodyne',
    'Aplus_20deg_SEC',
    'Aplus_20deg_SQZangle',
    'Aplus_20deg_rot',
    'CE_RSE',
    'CE_SR'
]


@pytest.mark.parametrize('ifo_name', ifos)
def test_configs(ifo_name, fpath_join, tpath_join, pprint):
    ff = np.logspace(0, 4, 800)
    budget = gwinc.load_budget(fpath_join(ifo_name + '.yaml'), freq=ff)
    # ifo = Struct.from_file(ifo_name + '.yaml')

    # zeta_orig = budget.ifo.Optics.Quadrature.dc
    # budget.ifo.Optics.Quadrature.dc = np.pi/2 - zeta_orig

    budget_o = q_old.Quantum(ifo=budget.ifo, freq=ff)
    budget_n = q_new.Quantum(ifo=budget.ifo, freq=ff)

    traces_o = budget_o.run()
    traces_n = budget_n.run()
    fig_o = traces_o.plot()
    fig_n = traces_n.plot()
    fig_o.savefig(tpath_join('budget_old.pdf'))
    fig_n.savefig(tpath_join('budget_new.pdf'))

    traces_dict = dict(traces_o)
    traces_dict.update({'Total': traces_o})
    allclose = []
    fig_comp, ax_comp = plt.subplots()
    fig_tot, ax_tot   = plt.subplots()
    for key1, trace1 in traces_dict.items():
        try:
            if key1 == 'Total':
                trace2 = traces_n
                ax = ax_tot

            else:
                trace2 = traces_n[key1]
                ax = ax_comp

            ratio = trace2.asd / trace1.asd
            pprint('{:s}: {:0.3e}'.format(key1, np.max(np.abs(ratio - 1))))

            if key1 != 'Total':
                allclose.append(np.allclose(ratio, 1, atol=1e-3))

            st = dict(trace1.style)
            st.pop('ls', None)
            st.pop('lw', None)
            st.pop('linewidth', None)
            ax.loglog(trace1.freq, trace2.asd / trace1.asd, lw=3, **st)

        except KeyError:
            pass

    for ax in [ax_comp, ax_tot]:
        ax.legend(ncol=2)
        ax.set_xlim(ff[0], ff[-1])
        ax.grid(True, which='major', alpha=0.5)
        ax.grid(True, which='minor', alpha=0.2)

    fig_comp.tight_layout()
    fig_comp.savefig(tpath_join('traces_ratio.pdf'))
    fig_tot.tight_layout()
    fig_tot.savefig(tpath_join('total_ratio.pdf'))

    assert np.all(allclose)
