import numpy as np

def plot_trace(
        trace,
        ax=None,
        psd=None,
        dbs=False,
        ylim_limits=(None, None),
        legend_kw={},
        ylim=None,
        **kwargs
):
    """Plot a GWINC BudgetTrace noise budget from calculated noises

    If an axis handle is provided it will be used for the plot.

    Returns the figure handle.

    """
    if ax is None:
        import matplotlib.pyplot as plt
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
    else:
        fig = ax.figure

    if psd is None:
        if dbs:
            psd = True
        else:
            psd = False

    if psd:
        total = trace.psd
        if ylim is None:
            ylim = [min(total)/30, max(total)]
    else:
        total = trace.asd
        if ylim is None:
            ylim = [min(total)/10, max(total)]
    ylim = list(ylim)

    if ylim_limits[1] is not None:
        ylim[1] = max(ylim[1], ylim_limits[1])

    if ylim_limits[0] is not None:
        ylim[0] = min(ylim[0], ylim_limits[0])

    style = dict(
        color='#000000',
        alpha=0.6,
        linewidth=4,
    )
    style.update(getattr(trace, 'style', {}))
    if 'color' not in style and 'c' not in style:
        style['color'] = '#000000'
    if 'alpha' not in style:
        style['alpha'] = 0.6
    if 'linewidth' not in style and 'lw' not in style:
        style['linewidth'] = 4
    if 'label' in style:
        style['label'] = 'Total ' + style['label']
    else:
        style['label'] = 'Total'
    ax.loglog(trace.freq, total, **style)

    for name, strace in trace.items():
        style = strace.style
        if 'label' not in style:
            style['label'] = name
        if 'linewidth' not in style and 'lw' not in style:
            style['linewidth'] = 3
        if psd:
            ax.loglog(trace.freq, strace.psd, **style)
        else:
            ax.loglog(trace.freq, strace.asd, **style)

    ax.grid(
        True,
        which='major',
        linewidth=0.5,
        ls='-',
        alpha=0.5,
    )

    ax.grid(
        True,
        which='minor',
        linewidth=0.5,
        ls='-',
        alpha=0.2,
    )

    ax.autoscale(enable=True, axis='y', tight=True)
    ax.set_ylim(ylim)
    ax.set_xlim(trace.freq[0], trace.freq[-1])
    ax.set_xlabel('Frequency [Hz]')
    if 'ylabel' in kwargs:
        ax.set_ylabel(kwargs['ylabel'])
    if 'title' in kwargs:
        ax.set_title(kwargs['title'])

    if dbs:
        from matplotlib import ticker
        ax.yaxis.set_major_locator(ticker.LogLocator(base=10, subs=(
            1,
            10**(-3/10.),
            10**(-6/10.),
        )))
        ax.yaxis.set_minor_locator(ticker.LogLocator(base=10, subs=(
            10**(-1/10.),
            10**(-2/10.),
            10**(-3/10.),
            10**(-4/10.),
            10**(-5/10.),
            10**(-6/10.),
            10**(-7/10.),
            10**(-8/10.),
            10**(-9/10.),
        )))
        ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda v, ofs: '{:.1f}'.format(10*np.log10(v))))
        ax.yaxis.set_minor_formatter(ticker.NullFormatter())

        #currently, adding the loss levels requires a twinx hack
        #by setting similar enough parameters that the ax2 has the same
        #scale as ax.
        if None not in ylim and 'ylabel2' in kwargs:
            ax2 = ax.twinx()
            ax2.set_yscale('log')
            ax2.yaxis.set_major_locator(ticker.LogLocator(base=10))
            ax2.yaxis.set_minor_formatter(ticker.NullFormatter())
            ax2.set_ylim(ylim)
            ax2.set_ylabel(kwargs['ylabel2'])
            #ax.yaxis.set_minor_locator()

    # putting legend after twinx robustly fixes zorder issue
    ax.legend(
        ncol=2,
        fontsize='small',
    )

    return fig


# FIXME: deprecate
plot_noise = plot_trace
plot_budget = plot_trace
