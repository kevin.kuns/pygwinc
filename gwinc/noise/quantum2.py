'''Functions to calculate quantum noise

'''
from __future__ import division
import numpy as np
from collections.abc import Sequence

from .. import logger
from .. import const
from ..struct import Struct

from .. import suspension
from .. import nb

from .quantum_lib import (
    Vnorm_sq,
    adjoint,
    matrix_stack,
    mats_planewave,
    mats_mode_mismatch,
)

pi2j = 2j * np.pi
NaN = float('NaN')

def shotrad_debug(freq, ifo, **kwargs):
    from gwinc.nb import _precomp_recurse_mapping
    _precomp = dict()
    pc = _precomp_recurse_mapping(precomp_quantum, freq = freq, ifo = ifo, _precomp = _precomp)
    pc.update(kwargs)
    retB = precomp_quantum(freq, ifo, **pc)
    sr = retB.ret
    return sr


@nb.precomp(sustf=suspension.precomp_suspension)
def precomp_quantum(f, ifo, sustf, **kwargs):
    from ..ifo import noises
    pcQ    = Struct()
    power = noises.ifo_power(ifo)
    ret   = shotrad(f, ifo, sustf, power, **kwargs)
    PSDdisplacement = ret.PSDdisplacement

    pcQ.ASvac     = PSDdisplacement * ret.ASquantumAll()
    AS    = ret.ASbudget
    # add noise from all filter cavities
    FCAll = np.sum([psd for key, psd in AS.items() if 'FCtrans' in key], axis=0)
    pcQ.SEC       = PSDdisplacement * AS['lossSEC']
    pcQ.Arm       = PSDdisplacement * AS['lossARM']
    pcQ.Injection = PSDdisplacement * AS['Loss_injection']
    pcQ.PD        = PSDdisplacement * AS['Loss_readout']
    pcQ.FC        = PSDdisplacement * FCAll
    pcQ.MM        = PSDdisplacement * AS['lossMM']
    pcQ.PSDdisplacement     = PSDdisplacement

    pcQ.Gamma     = ret['Gamma_IFO']
    pcQ.ret       = ret
    return pcQ

class QuantumAS(nb.Noise):
    """Quantum vacuum from the AS port

    """
    style = dict(
        label='AS Port Vacuum',
        color='xkcd:emerald green'
    )

    @nb.precomp(quantum=precomp_quantum)
    def calc(self, quantum):
        return quantum.ASvac


class QuantumArm(nb.Noise):
    """Quantum vacuum due to arm cavity loss

    """
    style = dict(
        label='Arm Loss',
        color='xkcd:orange brown'
    )

    @nb.precomp(quantum=precomp_quantum)
    def calc(self, quantum):
        return quantum.Arm


class QuantumMM(nb.Noise):
    """Quantum vacuum due to mode mismatch

    """
    style = dict(
        label='Mode Mismatch',
        color='xkcd:magenta'
    )

    @nb.precomp(quantum=precomp_quantum)
    def calc(self, quantum):
        return quantum.MM


class QuantumSEC(nb.Noise):
    """Quantum vacuum due to SEC loss

    """
    style = dict(
        label='SEC Loss',
        color='xkcd:cerulean'
    )

    @nb.precomp(quantum=precomp_quantum)
    def calc(self, quantum):
        return quantum.SEC


class QuantumFilterCavity(nb.Noise):
    """Quantum vacuum due to filter cavity loss

    """
    style = dict(
        label='Filter Cavity Loss',
        color='xkcd:goldenrod'
    )

    @nb.precomp(quantum=precomp_quantum)
    def calc(self, quantum):
        return quantum.FC


class QuantumInjection(nb.Noise):
    """Quantum vacuum due to injection loss

    """
    style = dict(
        label='Injection Loss',
        color='xkcd:fuchsia'
    )

    @nb.precomp(quantum=precomp_quantum)
    def calc(self, quantum):
        return quantum.Injection


class QuantumReadout(nb.Noise):
    """Quantum vacuum due to readout loss

    """
    style = dict(
        label='Readout Loss',
        color='xkcd:mahogany'
    )

    @nb.precomp(quantum=precomp_quantum)
    def calc(self, quantum):
        return quantum.PD


class QuantumLoss(nb.Budget):
    """Quantum vacuum from all losses

    """
    style = dict(
        label='Quantum Losses',
        color='Grey',
    )

    noises = [
        QuantumMM,
        QuantumArm,
        QuantumSEC,
        QuantumFilterCavity,
        QuantumInjection,
        QuantumReadout,
    ]


class QuantumRelASSqz(nb.Noise):
    """Quantum vacuum from the AS port

    eta * (1 - Xi') * e^{-2r} * cos^2(phi + theta)
    """
    style = dict(
        label='AS Port SQZ',
        color='xkcd:emerald green',
    )

    @nb.precomp(quantum=precomp_quantum)
    def calc(self, quantum):
        return quantum.ret['etaS_sqz']


class QuantumRelASMisrotation(nb.Noise):
    """Quantum vacuum due to misrotation

    eta * S_+ * sin^2(phi + theta)
    """
    style = dict(
        label='SQZ misrotation*',
        color='xkcd:emerald green',
        ls = '--',
    )

    @nb.precomp(quantum=precomp_quantum)
    def calc(self, quantum):
        return quantum.ret['etaS_misrot']


class QuantumXiDephase(nb.Noise):
    """Quantum vacuum from the fundamental IFO dephasing

    Xi (not Xi')
    """
    style = dict(
        label='SQZ FC/IFO dephasing*',
        color='xkcd:emerald green',
        ls = ':',
    )

    @nb.precomp(quantum=precomp_quantum)
    def calc(self, quantum):
        return quantum.ret['Xi_IFO']


class QuantumXiBroadBand(nb.Noise):
    """Dephasing from broadband phase noise

    Term in Xi' from sqz phase noise
    """
    style = dict(
        label='SQZ Phase Noise*',
        color='xkcd:sea green',
        ls = '--',
    )

    @nb.precomp(quantum=precomp_quantum)
    def calc(self, quantum):
        return quantum.ret['Xi_SQZphase_noise']


class QuantumXiLO(nb.Noise):
    """Dephasing from LO phase noise

    Term in Xi' from LO phase noise
    """
    style = dict(
        label='LO Phase Noise*',
        color='mediumvioletred',
        ls = ':',
    )

    @nb.precomp(quantum=precomp_quantum)
    def calc(self, quantum):
        return quantum.ret['Xi_LOphase_noise']


class QuantumXiFC(nb.Noise):
    """Dephasing from filter cavity RMS length fluctuations

    Term in Xi' from filter cavity length fluctuations
    """
    style = dict(
        label='SQZ FC Length RMS*',
        #color='xkcd:emerald green'
        color='xkcd:goldenrod',
        ls = '--',
    )

    @nb.precomp(quantum=precomp_quantum)
    def calc(self, quantum):
        return quantum.ret['Xi_FC']


class QuantumXiSEC(nb.Noise):
    """Dephasing from SEC RMS length fluctuations

    Term in Xi' from SEC length fluctuations
    """
    style = dict(
        label='SQZ SEC Length RMS*',
        #color='xkcd:emerald green'
        color='xkcd:cerulean',
        ls = '--',
    )

    @nb.precomp(quantum=precomp_quantum)
    def calc(self, quantum):
        return quantum.ret['Xi_SEC']


class QuantumXiAsqzCal(nb.Calibration):
    """Calibrates dephasing into quantum noise relative to Gamma

    eta * e^{2r} * cos^2(phi + theta)

    When multiplied by the total effective dephasing Xi' (QuantumXi) and
    added to AS port SQZ (QuantumRelASSqz) gives

    eta * S_- + cos^2(phi + theta)
    """

    @nb.precomp(quantum=precomp_quantum)
    def calc(self, quantum):
        return quantum.ret['eta_Csq_asqz']


class QuantumASPortCal(nb.Calibration):
    """Optomechanical noise gain Gamma
    """
    @nb.precomp(quantum=precomp_quantum)
    def calc(self, quantum):
        return quantum.Gamma


class QuantumDisplacementCal(nb.Calibration):
    """Sensing function
    """
    @nb.precomp(quantum=precomp_quantum)
    def calc(self, quantum):
        return quantum.PSDdisplacement


class QuantumASPortInvCal(nb.Calibration):
    """Inverse of the optomechanical gain: 1/Gamma
    """
    @nb.precomp(quantum=precomp_quantum)
    def calc(self, quantum):
        return 1/quantum.Gamma

class QuantumDisplacementInvCal(nb.Calibration):
    """Inverse sensing function
    """
    @nb.precomp(quantum=precomp_quantum)
    def calc(self, quantum):
        return 1/quantum.PSDdisplacement


class QuantumXi(nb.Budget):
    """Total effective dephasing Xi'
    """
    style = dict(
        label='Dephasing',
        color='xkcd:dark red',
    )

    plot_style = dict(
        ylabel = 'Phase [rad]',
    )

    noises = [
        QuantumXiDephase,
        QuantumXiBroadBand,
        # QuantumXiLO,
        QuantumXiFC,
        QuantumXiSEC,
    ]


class QuantumSqz(nb.Budget):
    """Quantum vacuum from the AS port

    """
    style = dict(
        label='AS Port Sqz',
        color='xkcd:emerald green'
    )

    noises = [
        QuantumRelASSqz,
        QuantumRelASMisrotation,
    ]

    noises_forward = [
        (QuantumXi, QuantumXiAsqzCal),
    ]

    calibrations = [
        QuantumASPortCal,
        QuantumDisplacementCal,
    ]


class Quantum(nb.Budget):
    """Total quantum vacuum including loss

    """
    style = dict(
        label='Quantum Vacuum',
        color='#ad03de',
    )

    noises_forward = [
        QuantumSqz,
        QuantumLoss,
    ]


class QuantumRelShotNoise(nb.Budget):
    """Quantum vacuum relative to broadband shot noise hbar*omega/2

    """
    style = dict(
        label='Quantum Vacuum',
        color='#ad03de',
    )

    plot_style = dict(
        psd=True,
        dbs=True,
        ylim_limits=(1e-2, 2),
        legend_kw=dict(fontsize='x-small', ncol=3),
        ylabel="Noise relative to unsqueezed vacuum [dB]",
        ylabel2="Effective Loss Level",
    )

    noises = [
        QuantumRelASSqz,
        QuantumRelASMisrotation,
    ]

    noises_forward = [
    ]

    noises_forward = [
        (QuantumXi, QuantumXiAsqzCal),
        (QuantumLoss, QuantumDisplacementInvCal),
    ]


# class QuantumRelGamma(nb.Budget):
#     """Quantum Vacuum

#     """
#     style = dict(
#         label='Quantum Vacuum',
#         color='#ad03de',
#     )

#     plot_style = dict(
#         psd=True,
#         dbs=True,
#         ylim_limits=(1e-2, 2),
#         legend_kw=dict(fontsize='x-small', ncol=3),
#     )

#     noises = [
#         QuantumRelASSqz,
#         QuantumRelASMisrotation,
#     ]

#     noises_forward = [
#         (QuantumXi, QuantumXiAsqzCal),
#         (QuantumLoss, QuantumDisplacementInvCal),
#     ]

#     calibrations = [
#         QuantumASPortInvCal,
#     ]


class QuantumRelGamma(nb.Budget):
    """Quantum vacuum relative to the optomechanical noise gain Gamma

    """
    style = dict(
        label='Quantum Vacuum',
        color='#ad03de',
    )

    plot_style = dict(
        psd=True,
        dbs=True,
        ylim_limits=(1e-2, 2),
        legend_kw=dict(fontsize='x-small', ncol=3),
        ylabel="Noise relative to unsqueezed vacuum [dB]",
        ylabel2="Effective Loss Level",
    )

    noises = []

    noises_forward = [
        QuantumSqz,
        QuantumLoss,
    ]

    calibrations = [
        QuantumDisplacementInvCal,
        QuantumASPortInvCal,
    ]


def standardize_params(ifo):
    """Determine squeezer type, if any, and extract common parameters

    Returns a struct with the following attributes:
    SQZ_DB: squeezing in dB
    ANTISQZ_DB: anti-squeezing in dB
    alpha: freq Indep Squeeze angle [rad]
    lambda_in: loss to squeezing before injection [Power]
    LO_RMS: quadrature noise [rad]
    eta_IFO: homodyne angle [rad]
    """
    params = Struct()

    if 'Squeezer' not in ifo:
        sqzType = 'None'
    elif ifo.Squeezer.AmplitudedB == 0:
        sqzType = 'None'
    else:
        sqzType = ifo.Squeezer.get('Type', 'Freq Independent')

    params.sqzType = sqzType
    if sqzType == 'None':
        params.SQZ_DB     = 0
        params.ANTISQZ_DB = 0
        params.alpha      = 0
        params.lambda_in  = 0
        params.LO_RMS     = 0
        params.SQZ_RMS    = 0
    else:
        params.SQZ_DB     = ifo.Squeezer.AmplitudedB
        params.ANTISQZ_DB = ifo.Squeezer.get('AntiAmplitudedB', params.SQZ_DB)
        params.alpha      = ifo.Squeezer.SQZAngle
        params.lambda_in  = ifo.Squeezer.InjectionLoss
        params.LO_RMS     = ifo.Squeezer.get('LOAngleRMS', 0)
        params.SQZ_RMS    = ifo.Squeezer.get('SQZAngleRMS', 0)

    # Homodyne Readout phase
    LO_angle_orig = ifo.Optics.get('Quadrature', Struct()).get('dc', None)
    # print(LO_angle_orig - np.pi/2)

    ifoRead = ifo.get('Squeezer', Struct()).get('Readout', None)
    if ifoRead is None:
        LO_angle = LO_angle_orig
        if LO_angle_orig is None:
            raise Exception("must add Quadrature.dc or Readout...")

    elif ifoRead.Type == 'DC':
        LO_angle = (
            np.sign(ifoRead.fringe_side)
            * np.arccos((ifoRead.defect_PWR_W / ifoRead.readout_PWR_W)**.5)
        )

    elif ifoRead.Type == 'Homodyne':
        LO_angle = ifoRead.Angle
    else:
        raise Exception("Unknown Readout Type")

    if LO_angle_orig is not None and LO_angle_orig != LO_angle:
        raise Exception("Quadrature.dc inconsistent with Readout LO_angle")

    if ifoRead is not None:
        params.follow_fringe = ifoRead.get('follow_fringe', False)
    else:
        #this is not a sane default, but it is the default of GWINC
        params.follow_fringe = False

    params.LO_angle = LO_angle - np.pi/2

    return params


def shotrad(
    freq,
    ifo,
    sustf,
    power,
    debug = False
):
    #defaults
    self = Struct()
    ####################
    #extract parameters
    ####################
    F_Hz           = freq
    lambda_        = ifo.Laser.Wavelength               # Laser Wavelength [m]
    k              = 2 * np.pi / lambda_
    params         = standardize_params(ifo)
    sqz_type       = params.sqzType
    FC_use         = (params.sqzType == 'Freq Dependent')

    parm_W         = power.parm

    SQZ_DB         = params.SQZ_DB
    ASQZ_DB        = params.ANTISQZ_DB
    SQZ_angle_rad  = params.alpha
    HD_angle_rad   = params.LO_angle
    LO_angle_RMS   = params.LO_RMS
    SQZ_angle_RMS  = params.SQZ_RMS
    Loss_injection = params.lambda_in
    Loss_readout   = 1 - ifo.Optics.PhotoDetectorEfficiency

    bsloss         = ifo.Optics.BSLoss                           # BS Loss [Power]
    mismatch       = (1 - ifo.Optics.coupling) + ifo.TCS.SRCloss # Mismatch

    ############
    # Initial Setup
    ############
    sqzV = 10**(-SQZ_DB/10.)
    asqzV = 10**(ASQZ_DB/10.)

    mats = MatsHelper()
    mats.H['AS'] = mats.olib.Id

    #apply the propagation loss
    L_inj_t = (1 - Loss_injection)**0.5
    mats.update_scalar(L_inj_t)
    mats.T['Loss_injection'] = mats.olib.Id * Loss_injection**0.5

    access = Struct()

    ###########
    #Filter Cavity
    ###########

    if FC_use and ('FilterCavity' in ifo.Squeezer):
        if not isinstance(ifo.Squeezer.FilterCavity, Sequence):
            fc_list = [ifo.Squeezer.FilterCavity]
        else:
            fc_list = ifo.Squeezer.FilterCavity
        logger.debug('  Applying %d input filter cavities' % len(fc_list))
        for idx, ifo_fc in enumerate(fc_list):
            access['FC{}'.format(idx)] = Struct()
            apply_filter_cavity(
                lambda_     = lambda_,
                F_Hz        = F_Hz,
                mats        = mats,
                length_m    = ifo_fc.L,
                lengthRMS_m = ifo_fc.get('Lrms', 0),
                T_IC        = ifo_fc.Ti,
                L_RT        = (1 - (1 - ifo_fc.Te) * (1 - ifo_fc.Lrt)),
                det_Hz      = ifo_fc.fdetune,
                rot         = ifo_fc.Rot,
                L_MM        = ifo_fc.get('L_mm', 0),
                psi_MM      = ifo_fc.get('psi_mm', 0),
                gouy_rad    = ifo_fc.get('Gouy_rad', None),
                name_Lrms   = 'FCrmsL{}'.format(idx),
                name_trans  = 'FCtrans{}'.format(idx),
                access      = access['FC{}'.format(idx)],
            )

    apply_interferometer(
        lambda_           = lambda_,
        F_Hz              = F_Hz,
        mats              = mats,
        tst_suscept       = sustf.tst_suscept,
        T_ITM             = ifo.Optics.ITM.Transmittance,
        T_SRM             = ifo.Optics.SRM.Transmittance,
        L_SRM_m           = ifo.Optics.SRM.CavityLength,
        L_ARM_m           = ifo.Infrastructure.Length,
        Loss_ARM          = 1 - (1 - ifo.Optics.Loss)**2 * (1 - ifo.Optics.ETM.Transmittance),
        Loss_SRC          = 1 - (1 - mismatch) * (1 - bsloss),  # SR cavity loss [Power]
        parm_W            = parm_W,
        SEC_lengthRMS_m   = ifo.Optics.SRM.get('LengthRMS', 0),
        ARM_detune_rad    = 0,
        SRM_detune_rad    = ifo.Optics.SRM.Tunephase/2,
        SRC_gouy_rad      = ifo.Optics.SRM.get('SRCGouy_rad', None),
        ARM_gouy_rad      = arm_gouyRT(
            ifo.Optics.Curvature.ITM,
            ifo.Infrastructure.Length,
            ifo.Optics.Curvature.ETM,
        ),  # one-way Gouy phase of the LG10 mode: 2 * gouyRT / 2
        MM_SRC_ARM_L      = ifo.Optics.get('MM_ARM_SRC', 0),
        MM_SRC_ARM_rad    = ifo.Optics.get('MM_ARM_SRCphi', 0),
        MM_IFO_OMC_L      = ifo.Optics.get('MM_IFO_OMC', 0),
        MM_IFO_OMC_rad    = ifo.Optics.get('MM_IFO_OMCphi', 0),
        MM_SQZ_OMC_L      = 0 if sqz_type is 'None' else ifo.Squeezer.get('MM_SQZ_OMC', 0),
        MM_SQZ_OMC_rad    = 0 if sqz_type is 'None' else ifo.Squeezer.get('MM_SQZ_OMCphi', 0),
        num_fold_mirror   = ifo.Infrastructure.get('num_fold_mirror', 0),
        direct_mm_sqz_ifo = True if sqz_type == 'None' else ifo.Squeezer.get('direct_mm_sqz_ifo', False),
        is_OPD            = ifo.Optics.get('is_OPD', False),
        access            = access,
    )

    if FC_use and ('OutFilterCavity' in ifo.Squeezer):
        if not isinstance(ifo.Squeezer.OutFilterCavity, Sequence):
            fc_list = [ifo.Squeezer.OutFilterCavity]
        else:
            fc_list = ifo.Squeezer.OutFilterCavity
        logger.debug('  Applying %d input filter cavities' % len(fc_list))
        for idx, ifo_fc in enumerate(fc_list):
            access['OutFC{}'.format(idx)] = Struct()
            apply_filter_cavity(
                lambda_     = lambda_,
                mats        = mats,
                F_Hz        = F_Hz,
                length_m    = ifo_fc.L,
                lengthRMS_m = ifo_fc.Lrms,
                T_IC        = ifo_fc.Ti,
                L_RT        = (1 - (1 - ifo_fc.Te) * (1 - ifo_fc.Lrt)),
                det_Hz      = ifo_fc.fdetune,
                rot         = ifo_fc.Rot,
                L_MM        = ifo_fc.get('L_mm', 0),
                psi_MM      = ifo_fc.get('psi_mm', 0),
                gouy_rad    = ifo_fc.get('Gouy_rad', None),
                name_Lrms   = 'OutFCrmsL{}'.format(idx),
                name_trans  = 'OutFCtrans{}'.format(idx),
                access      = access['OutFC{}'.format(idx)],
            )

    #apply the propagation loss
    L_read_t = (1 - Loss_readout)**0.5
    mats.update_scalar(L_read_t)
    mats.T['Loss_readout'] = mats.olib.diag(Loss_readout**0.5)

    ####################
    # Interferometer Sensing
    ####################

    def prepareLOa(loc):
        if params.follow_fringe:
            LOa = adjoint(mats.L[loc] @ mats.olib.LO(HD_angle_rad))
            LOa[..., 2:] = 0
            LOa = LOa / Vnorm_sq(LOa)**0.5
        else:
            LOa = adjoint(mats.olib.LO(HD_angle_rad))
        return LOa
    LOa = prepareLOa('AS')

    delta_LO_rad = 1e-6
    DpLOa = adjoint(mats.olib.LO(HD_angle_rad + delta_LO_rad))

    #Take the transmission from phase modulation at the end mirror of the arm
    LOdotArmPhase = (LOa @ mats.H['ArmTrans'])[..., 0, 1]  # v^\dagger U_O t_A e_{p0}

    BS_factor = 1/2**0.5
    # scalar s in (E22)
    d_sense = BS_factor * (2 * k * LOdotArmPhase * parm_W**0.5)

    omega = k * const.c
    Qnoise = const.hbar * omega / 2
    PSDdisplacement = Qnoise / abs(d_sense)**2  # \hbar\omega/2 * G * L_A^2 in (E23)

    ####################
    # Outputs
    ####################

    ASbudget = {}
    Lambda_IRO = 0

    for _k in mats.T:
        LOSSIFOOUTx = Vnorm_sq(LOa @ mats.T[_k])
        ASbudget[_k] = LOSSIFOOUTx
        Lambda_IRO = Lambda_IRO + LOSSIFOOUTx

    LOdotAS = (LOa @ mats.H['AS'])

    #np.angle(a + bj) == np.arctan2(b, a)
    IFOangle_rad_approx = np.arctan((LOdotAS[..., 0, 0] / LOdotAS[..., 0, 1]).real)

    SQZ_Vsq             = Vnorm_sq(LOdotAS)
    #this isn't quite the definition used in the paper, as that one is a bit
    #inconvenient. By this I mean that the computation is the same,
    #but here it is calculated after all of the output losses, so Gamma depends
    #on the output loss. The paper chooses to compute it right after the IFO
    #so Gamma is independent, that is nice, but if you have an output filter
    #cavity, then you have to include output filter cavity losses in Gamma no
    #matter what..
    Gamma_IFO             = SQZ_Vsq + Lambda_IRO
    theta_IFO, Xi_IFO, etaGamma_IFO = thetaXietaGamma(LOdotAS)
    eta_IFO               = etaGamma_IFO / Gamma_IFO

    ASquantumAll = lambda : Vnorm_sq(LOdotAS @ mats.olib.Mrotation(SQZ_angle_rad) @ mats.olib.SQZ(sqzV, asqzV))
    if debug:
        #this debug segment tests that the SVD method recovers the original
        #calculation purely using the matrices
        Nsqz         = Vnorm_sq(LOdotAS @ mats.olib.Mrotation(SQZ_angle_rad) @ mats.olib.SQZ(sqzV, asqzV))
        Csq          = np.cos(SQZ_angle_rad + theta_IFO)**2
        Ssq          = np.sin(SQZ_angle_rad + theta_IFO)**2
        S_std_sqz    = Csq * ((1 - Xi_IFO) * sqzV + Xi_IFO * asqzV)
        S_std_misrot = Ssq * ((1 - Xi_IFO) * asqzV + Xi_IFO * sqzV)
        S_std        = S_std_sqz + S_std_misrot

        from numpy.testing import assert_allclose
        assert_allclose(etaGamma_IFO * S_std, Nsqz)

        #test equation 38
        mq, mp = LOdotAS[..., 0, 0],  LOdotAS[..., 0, 1]
        Xi_calc = 1/2 - (((abs(mq)**2 - abs(mp)**2)**2 + 4 * (mq * mp.conjugate()).real**2) / 4 / (abs(mq)**2 + abs(mp)**2)**2)**0.5
        #assert_allclose(Xi_calc, Xi_IFO)

        LOdiffRad = thetaXietaGamma(adjoint(mats.L['AS'] @ mats.olib.LO(HD_angle_rad)))[0] - HD_angle_rad + np.pi/2

    # mode mismatch loss is not included in Lambda_IRO. It is the difference between the
    # norm over the full product space of HOMs and the norm over just the fundamental mode
    # i.e. eta*Gamma = |mp|^2 + |mq|^2 while SQZ_Vsq has contributions from the HOMs if present
    ASbudget['lossMM'] = SQZ_Vsq - etaGamma_IFO

    # Xi_accum will be the total effective dephasing (Xi' in (9))
    # start with the fundamental dephasing from the IFO as calculated in (A7)
    Xi_accum    = Xi_IFO

    def XiUpdate(fullXi, subXi):
        """Add dephasing from other mechanisms to the total effective dephasing using (B4)

        Inputs:
        fullXi: the effective dephasing up to this point (Xi in (B4))
        subXi: the new statistical fluctions (phi_rms in (B4))

        Returns:
        fullXi: the updated effective dephasing (Xi' in (B4))
        subXi: the contribution to the effective dephasing from this mechanism
        """
        subXi  = subXi - 2 * fullXi * subXi
        fullXi = fullXi + subXi
        return fullXi, subXi

    Xi_accum, Xi_SQZphase_noise = XiUpdate(Xi_accum, SQZ_angle_RMS**2)

    Dptheta_IFO, DpXi_IFO, _ = thetaXietaGamma(LOa @ mats.olib.Mrotation(delta_LO_rad) @ mats.H['AS'] @ mats.olib.Mrotation(-delta_LO_rad))
    dLOtheta_IFO = angle_derivative(Dptheta_IFO, theta_IFO, LO_angle_RMS / delta_LO_rad)
    Xi_accum, Xi_LOphase_noise = XiUpdate(Xi_accum, dLOtheta_IFO**2)

    # Dephasing noise from SEC and FC length RMS
    # The TFs in Hmats are computed at the nominal \Delta L tunings
    # The TFs with a shift in the name are computed at \Delta L + dx and Lrms/dx is stored in Hshifts

    def dephasing_from_length_rms(loc, Xi_eff):
        """Calculate the dephasing from RMS length fluctuations

        Inputs:
        loc: the location of the length fluctuations
        Xi_eff: the dephasing from these fluctuations is added to Xi_eff

        Returns:
        Xi_eff: the updated effective dephasing
        subXi: the contribution to Xi_eff from rms fluctuations at loc
        """
        # theta_IFO is the rotation at the nominal \Delta L
        # theta_shift is the rotation at \Delta L + dx
        theta_shift, _, _ = thetaXietaGamma(prepareLOa(loc) @ mats.H[loc])
        dphase = angle_derivative(theta_shift, theta_IFO, mats.Hshifts[loc])
        Xi_eff, subXi = XiUpdate(Xi_eff, dphase**2)
        return Xi_eff, subXi

    #accumulate all phase noise from FCs
    Xi_FC = 0
    for _k in mats.FCshifts:
        Xi_FC, _ = dephasing_from_length_rms(_k, Xi_FC)
    Xi_accum, Xi_FC = XiUpdate(Xi_accum, Xi_FC)

    if 'ASshiftSEC' in mats.Hshifts:
        Xi_accum, Xi_SEC = dephasing_from_length_rms('ASshiftSEC', Xi_accum)
    else:
        Xi_SEC = 0

    # #accumulate all phase noise from FCs
    # Xi_FC = 0
    # for _k in mats.FCshifts:
    #     dphase = angle_derivative(thetaXietaGamma(prepareLOa(_k) @ mats.H[_k])[0], theta_IFO, mats.Hshifts[_k])
    #     Xi_FC, _ = XiUpdate(Xi_FC, dphase**2)
    # Xi_accum, Xi_FC = XiUpdate(Xi_accum, Xi_FC)

    # if 'ASshiftSEC' in mats.Hshifts:
    #     dphase = angle_derivative(thetaXietaGamma(prepareLOa('ASshiftSEC') @ mats.H['ASshiftSEC'])[0], theta_IFO, mats.Hshifts['ASshiftSEC'])
    #     Xi_accum, Xi_SEC = XiUpdate(Xi_accum, dphase**2)
    # else:
    #     Xi_SEC = 0

    # theta_IFO = 0
    Csq                    = np.cos(SQZ_angle_rad + theta_IFO)**2
    Ssq                    = np.sin(SQZ_angle_rad + theta_IFO)**2
    etaS_sqz               = eta_IFO * Csq * sqzV * (1 - Xi_accum)
    etaS_asqz              = eta_IFO * Csq * asqzV * Xi_accum

    eta_Csq_asqz           = eta_IFO * Csq * asqzV
    etaS_Xi_FC_IFO         = eta_Csq_asqz * Xi_IFO
    etaS_Xi_SQZphase_noise = eta_Csq_asqz * Xi_SQZphase_noise
    etaS_Xi_LOphase_noise  = eta_Csq_asqz * Xi_LOphase_noise
    etaS_Xi_Filter_Cavity  = eta_Csq_asqz * Xi_FC
    etaS_Xi_SEC            = eta_Csq_asqz * Xi_SEC

    sqzVfull               = (1 - Xi_accum) * asqzV + Xi_accum * sqzV
    etaS_misrot            = eta_IFO * Ssq * sqzVfull

    etaS_full = etaS_sqz + etaS_asqz + etaS_misrot

    if debug:
        return Struct(locals())
    else:
        return Struct(locals())


def apply_interferometer(
    F_Hz,
    lambda_,
    mats,
    tst_suscept,
    parm_W,
    T_ITM,
    T_SRM,
    L_SRM_m,
    L_ARM_m,
    Loss_ARM,
    Loss_SRC,
    SEC_lengthRMS_m,
    SRM_detune_rad,
    SRC_gouy_rad,
    ARM_gouy_rad,
    MM_SRC_ARM_L,
    MM_SRC_ARM_rad,
    MM_IFO_OMC_L,
    MM_IFO_OMC_rad,
    MM_SQZ_OMC_L,
    MM_SQZ_OMC_rad,
    ARM_detune_rad,
    num_fold_mirror,
    direct_mm_sqz_ifo,
    is_OPD,
    access,
):
    k_ = 2 * np.pi / lambda_

    if SRC_gouy_rad is None or ARM_gouy_rad is None:
        SRC_gouy_rad = NaN
        ARM_gouy_rad = NaN
        if MM_SRC_ARM_L != 0:
            raise RuntimeError("Cannot apply internal ARM/SRC mismatch without knowing ARM and SRC Gouy phases")
        ilib = mats_planewave
    else:
        if MM_SRC_ARM_L != 0 or MM_IFO_OMC_L != 0 or MM_SQZ_OMC_L != 0:
            ilib = mats_mode_mismatch
            logger.debug("USING GOUY")
        else:
            ilib = mats_planewave

    if MM_IFO_OMC_L != 0 or MM_SQZ_OMC_L != 0:
        mlib = mats_mode_mismatch
    else:
        mlib = ilib

    if mlib is mats_mode_mismatch and mats.olib is mats_planewave:
        mats.update_olib(mats_mode_mismatch)

    MM_ARM_SRC = ilib.MrotationMM(MM_SRC_ARM_L, MM_SRC_ARM_rad)
    #MM_SQZ_SRC = lib.MrotationMM(MM_SQZ_IFO_L, MM_SQZ_IFO_phi)
    MM_IFO_OMC = mlib.MrotationMM(MM_IFO_OMC_L, MM_IFO_OMC_rad)
    MM_SQZ_OMC = mlib.MrotationMM(MM_SQZ_OMC_L, MM_SQZ_OMC_rad)
    #MM_IFO_OMC = MM_SQZ_OMC @ MM_SQZ_SRC
    if direct_mm_sqz_ifo:
        MM_SQZ_SRC = mlib.MrotationMM(MM_SQZ_OMC_L, MM_SQZ_OMC_rad)
    else:
        MM_SQZ_OMC = mlib.MrotationMM(MM_SQZ_OMC_L, MM_SQZ_OMC_rad)
        MM_SQZ_SRC = mlib.Minv(MM_SQZ_OMC) @ MM_IFO_OMC
    access.MM_ARM_SRC = MM_ARM_SRC
    access.MM_IFO_OMC = MM_IFO_OMC
    access.MM_SQZ_OMC = MM_SQZ_OMC
    access.MM_SQZ_SRC = MM_SQZ_SRC

    #the funny exponent is to split the loss by half, putting some on the ITM
    #has barely any effect except for the signal
    rETM = (1 - Loss_ARM)**.25

    tITM = T_ITM**.5
    rITM = ilib.diag((1 - Loss_ARM)**.25 * (1 - T_ITM)**.5)

    tSRM = T_SRM**.5
    rSRM = (1 - T_SRM)**.5

    def SEC(SRM_detune_rad = SRM_detune_rad, F_Hz = F_Hz):
        if F_Hz is not None:
            #Radiation Pressure interaction in the quadrature basis
            #extra factor of 2 is for both ITM and ETM getting pushed
            kappa_internal = 2 * 8 * k_ * parm_W / const.c * tst_suscept
            # N folding mirrors in a folded interferometer increases the
            # effective susceptibility by a factor of (1 + 2N) since each are
            # double passed
            if num_fold_mirror:
                kappa_internal *= (1 + 2 * num_fold_mirror)
        else:
            F_Hz = 0
            kappa_internal = 0

        rETMK = rETM * ilib.RPNK(kappa_internal)
        ###########
        # Arm
        ###########
        # one way arm phase
        phaseARM = ilib.diag(np.exp(-pi2j * F_Hz * L_ARM_m / const.c)) @ ilib.Mrotation(-ARM_detune_rad, ARM_gouy_rad)
        # arm round-trip as of the end mirror
        rtARM = rETMK @ phaseARM @ rITM @ phaseARM
        # closed loop propagator as of the end mirror
        clARM = ilib.Minv(ilib.Id - rtARM)
        # arm reflectivity
        reflARM = rITM - (tITM * phaseARM @ clARM @ rETMK @ phaseARM * tITM)
        if is_OPD:
            reflARM = MM_ARM_SRC @ reflARM @ MM_ARM_SRC
        else:
            reflARM = MM_ARM_SRC @ reflARM @ np.linalg.inv(MM_ARM_SRC)

        ###########
        # SEC
        ###########

        #for RSE convention used here
        SRM_detune_rad = np.pi/2 + SRM_detune_rad

        # one way SRC phase
        phaseSRC = ilib.diag(np.exp(-pi2j * F_Hz * L_SRM_m / const.c)) @ ilib.Mrotation(-SRM_detune_rad, SRC_gouy_rad)
        # counter propagation applied to opposite side of SRM
        cSRC = ilib.Mrotation(np.pi/2, 0)
        # round-trip of the SRC as of the ITM/ARM reflection
        rtSRC = -rSRM * (reflARM @ phaseSRC @ phaseSRC) * (1 - Loss_SRC)**0.5
        # closed loop propagator as of the ITM/ARM reflection
        clSRC = ilib.Minv(ilib.Id - rtSRC)
        # SRC reflectivity
        reflSRC = cSRC @ (ilib.diag(rSRM) + (tSRM**2 * (1 - Loss_SRC)**0.5 * (phaseSRC @ clSRC @ reflARM @ phaseSRC))) @ cSRC
        #this has to be a -1 due to the convention of adding an additional rotation
        #the HOMs end up always seeing the additional phase shift from it and
        #getting the -1 applied.
        reflSRC = MM_IFO_OMC @ mlib.promote(-1, reflSRC) @ mlib.Minv(MM_SQZ_SRC)
        return Struct(
            rtARM    = rtARM,
            clARM    = clARM,
            reflARM  = reflARM,
            phaseARM = phaseARM,
            phaseSRC = phaseSRC,
            rtSRC    = rtSRC,
            clSRC    = clSRC,
            reflSRC  = reflSRC,
            cSRC     = cSRC,
        )

    sec    = SEC()
    sec0Hz = SEC(F_Hz = None)

    access.sec = sec

    transBS0Hz  = MM_IFO_OMC @ mlib.promote(0, sec0Hz.cSRC @ (tSRM * sec0Hz.phaseSRC) @ sec0Hz.clSRC)
    transBS0Hz = mats.olib.promote(0, transBS0Hz)
    mats.L.update({k: transBS0Hz for k in mats.H.keys()})

    # arm transmissivity
    transARM = MM_ARM_SRC @ (tITM * sec.phaseARM) @ sec.clARM

    # transmission from ETM through SRM
    transETM = MM_IFO_OMC @ mlib.promote(0, sec.cSRC @ (tSRM * sec.phaseSRC) @ sec.clSRC @ transARM)
    LtransSRC = Loss_SRC**0.5 * MM_IFO_OMC @ mlib.promote(0, tSRM * sec.cSRC @ sec.phaseSRC @ sec.clSRC)
    LtransARM = Loss_ARM**0.5 * transETM
    access.transETM = transETM
    access.transARM = transARM

    if SEC_lengthRMS_m != 0:
        #I don't like that this requires a copy of the code, but can't do it without yet
        delta_SEC_L = 1e-13
        sec_shift = SEC(SRM_detune_rad = SRM_detune_rad + k_ * delta_SEC_L)
        sec0Hz_shift = SEC(F_Hz = 0, SRM_detune_rad = SRM_detune_rad + k_ * delta_SEC_L)
        transBS0Hz_shift  = MM_IFO_OMC @ mlib.promote(0, (tSRM * sec0Hz.phaseSRC) @ sec0Hz.clSRC)
        ASshiftSEC = mats.olib.promote(-1, sec_shift.reflSRC) @ mats.H['AS']

    mats.update_matrix(mats.olib.promote(-1, sec.reflSRC))

    if SEC_lengthRMS_m != 0:
        mats.H['ASshiftSEC'] = ASshiftSEC
        mats.L['ASshiftSEC'] = mats.olib.promote(-1, transBS0Hz_shift)
        mats.Hshifts['ASshiftSEC'] = SEC_lengthRMS_m / delta_SEC_L

    #add in the transmission/cavity loss
    mats.T['lossSEC'] = mats.olib.promote(0, LtransSRC)
    mats.T['lossARM'] = mats.olib.promote(0, LtransARM)

    mats.H['ArmTrans'] = mats.olib.promote(0, transETM)

    return


def apply_filter_cavity(
    F_Hz,
    lambda_,
    mats,
    length_m,
    lengthRMS_m,
    T_IC,
    L_RT,
    det_Hz,
    L_MM,
    psi_MM,
    gouy_rad = NaN,
    rot        = 0,
    name_Lrms  = 'FCrmsL',
    name_trans = 'FCtrans',
    access     = Struct(),
):
    k = 2 * np.pi / lambda_
    if gouy_rad is None:
        gouy_rad = NaN
        ilib = mats_planewave
        if L_MM == 0:
            mlib = mats_planewave
        else:
            mlib = mats_mode_mismatch
    else:
        if L_MM == 0:
            ilib = mats_planewave
        else:
            ilib = mats_mode_mismatch
        mlib = ilib

    if mlib is mats_mode_mismatch and mats.olib is mats_planewave:
        mats.update_olib(mats_mode_mismatch)

    FSR_Hz      = const.c / (2 * length_m)
    #increased frequency means a shorter length
    det_m       = -det_Hz / FSR_Hz * lambda_ / 2
    M_Lr        = ilib.diag((1 - L_RT)**.5)
    M_Lt        = ilib.diag(L_RT**.5)
    M_ICr       = ilib.diag((1 - T_IC)**.5)
    M_ICt       = ilib.diag(T_IC**0.5)

    def FC(det_m = det_m, F_Hz = F_Hz):
        """
        Helper function for the FC reflection. It is made a function so that it
        can be used a second time for the length RMS
        """
        # one way FC phase
        PHASE = ilib.diag(np.exp(-pi2j * F_Hz * length_m/const.c)) @ ilib.Mrotation(-k * det_m, gouy_rad)
        # round-trip as of the input coupler reflection
        RT = M_ICr @ PHASE @ M_Lr @ PHASE
        # closed loop propagator as of the input coupler reflection
        CL = ilib.Minv(ilib.Id - RT)
        # FC reflectivity
        REFL = M_ICr - M_ICt @ PHASE @ M_Lr @ PHASE @ CL @ M_ICt
        REFL_MM = mlib.MrotationMM(L_MM, psi_MM) @ mlib.promote(1, REFL) @ mlib.MrotationMM(L_MM, psi_MM, inv = True)
        if rot != 0:
            REFL_MM = mlib.Mrotation(-rot) @ REFL_MM
        return Struct(
            REFL  = REFL_MM,
            RT    = RT,
            PHASE = PHASE,
            CL    = CL,
        )

    fc = FC()
    fc0Hz = FC(F_Hz = 0)
    mats.update_LO(fc0Hz.REFL)
    access.fc = fc

    #now run a second time to establish the rotation derivative
    if lengthRMS_m != 0:
        delta_L = -1e-15
        fc_shift = FC(det_m = det_m + delta_L)
        SQZshiftFC = fc_shift.REFL @ mats.H['AS']
        mats.FCshifts.add(name_Lrms)
        if 'AS' in mats.L:
            fc0Hz_shift = FC(det_m = det_m + delta_L, F_Hz = 0)
            mats.L[name_Lrms] = fc0Hz_shift.REFL @ mats.L['AS']

    mats.update_matrix(fc.REFL)
    #add in the transmission/cavity loss
    #could apply rot, but it doesn't do anything to vacuum
    FCLTrans = mats.olib.MrotationMM(L_MM, psi_MM) @ mats.olib.promote(0, M_ICt @ fc.PHASE @ fc.CL @ fc.PHASE @ M_Lt)
    mats.T[name_trans] = FCLTrans

    if lengthRMS_m != 0:
        mats.H[name_Lrms] = SQZshiftFC
        mats.Hshifts[name_Lrms] = lengthRMS_m / delta_L

    return


def arm_gouyRT(ROCi_m, L_m, ROCe_m):
    """Accumulated round-trip Gouy phase of a FP cavity

    Needs to be multiplied by (2p + l) for the Gouy phase of the LGpl mode
    and by (n + m) for the Gouy phase of the HGnm mode
    """
    L = matrix_stack([[1, L_m], [0, 1]])
    M2 = matrix_stack([[1., 0.], [-2. / ROCe_m, 1]])
    M1 = matrix_stack([[1., 0.], [-2. / ROCi_m, 1]])
    M = L @ M2 @ L @ M1
    A = M[0, 0]
    B = M[0, 1]
    D = M[1, 1]
    return np.sign(B) * np.arccos((A + D)/2)


class MatsHelper(object):
    def __init__(self):
        self.H = {}
        self.T = {}
        self.L = {}
        self.Hshifts = {}
        self.FCshifts = set()

        #this starts as the smallest, but will get promoted on the first object that
        #introduces mode mistmatch, or eventually misalignment
        self.olib = mats_planewave

    def update_matrix(self, H):
        self.H.update({k: H @ M for k, M in self.H.items()})
        self.T.update({k: H @ M for k, M in self.T.items()})

    def update_LO(self, H):
        self.L.update({k: H @ M for k, M in self.L.items()})

    def update_scalar(self, s):
        self.H.update({k: s * M for k, M in self.H.items()})
        self.T.update({k: s * M for k, M in self.T.items()})

    def update_olib(self, olib):
        self.olib = olib
        self.H.update({k: olib.promote(1, M) for k, M in self.H.items()})
        self.T.update({k: olib.promote(0, M) for k, M in self.T.items()})


def angle_derivative(A, B, d):
    return ((A - B + np.pi/2) % np.pi - np.pi/2) * d


def thetaXietaGamma(LOdotAS):
    """
    Equations 25, 26, and 27 from

    https://doi.org/10.1103/PhysRevD.105.122005
    """
    m_q = LOdotAS[..., 0, 0]
    m_p = LOdotAS[..., 0, 1]
    # m_real = matrix_stack([[
    #     m_q.real, m_q.imag
    # ], [
    #     m_p.real, m_p.imag
    # ]])

    # etaGamma = abs(LOdotAS[..., 0, 0])**2 + abs(LOdotAS[..., 0, 1])**2  # (A5)
    # U, S, V = np.linalg.svd(m_real)  # (A3)
    # Xi = S[..., 1]**2 / etaGamma  # (A7)
    # theta = -np.pi/2 - np.arctan2(U[..., 1, 0], U[..., 0, 0])
    # #theta = theta % np.pi
    # #theta = (theta + 1 * np.pi/4) % np.pi - 1 * np.pi/4
    m_p2 = np.abs(m_p)**2
    m_q2 = np.abs(m_q)**2
    etaGamma = m_p2 + m_q2
    theta = 0.5 * np.angle((m_p + 1j * m_q) / (m_p - 1j * m_q))
    num = (m_p2 - m_q2)**2 + 4 * np.real(m_q * m_p.conjugate())**2
    den = 4 * (m_p2 + m_q2)**2
    Xi = 1/2 - np.sqrt(num / den)
    return theta, Xi, etaGamma
