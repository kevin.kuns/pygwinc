from gwinc.ifo import PLOT_STYLE
from gwinc import noise
from gwinc import nb
from gwinc.ifo.noises import Strain

from gwinc.noise.quantum2 import (
    Quantum,
    QuantumRelShotNoise,
    QuantumRelGamma,
    QuantumDisplacementInvCal,
    QuantumASPortInvCal,
    QuantumXi,
)

class aLIGO(nb.Budget):

    name = 'Advanced LIGO'

    noises = [
        Quantum,
        noise.seismic.Seismic,
        noise.newtonian.Newtonian,
        noise.suspensionthermal.SuspensionThermal,
        noise.coatingthermal.CoatingBrownian,
        noise.coatingthermal.CoatingThermoOptic,
        noise.substratethermal.SubstrateBrownian,
        noise.substratethermal.SubstrateThermoElastic,
        noise.residualgas.ResidualGas,
    ]

    calibrations = [
        Strain,
    ]

    plot_style = PLOT_STYLE


class TotalRelGamma(nb.Budget):
    """
    Total noise relative to unsqueezed vacuum
    Includes the optomechanical gain.
    """

    name = 'Advanced LIGO'

    plot_style = dict(
        psd=True,
        dbs=True,
        ylim_limits=(1e-2, 2),
        legend_kw=dict(fontsize='x-small', ncol=3),
        ylabel="Noise relative to unsqueezed vacuum",
        ylabel2="Effective Loss Level",
    )

    noises = [
        Quantum,
        noise.seismic.Seismic,
        noise.newtonian.Newtonian,
        noise.suspensionthermal.SuspensionThermal,
        noise.coatingthermal.CoatingBrownian,
        noise.coatingthermal.CoatingThermoOptic,
        noise.substratethermal.SubstrateBrownian,
        noise.substratethermal.SubstrateThermoElastic,
        noise.residualgas.ResidualGas,
    ]

    calibrations = [
        QuantumDisplacementInvCal,
        QuantumASPortInvCal,
    ]
