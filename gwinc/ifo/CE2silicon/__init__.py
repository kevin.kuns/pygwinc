from gwinc.ifo import PLOT_STYLE
from gwinc import noise
from gwinc import nb
from gwinc.ifo.noises import Strain

from gwinc.noise.quantum2 import (
    Quantum,
    QuantumRelShotNoise,
    QuantumRelGamma,
    QuantumDisplacementInvCal,
    QuantumASPortInvCal,
    QuantumXi,
)


class Newtonian(nb.Budget):
    """Newtonian Gravity

    """

    name = 'Newtonian'

    style = dict(
        label='Newtonian',
        color='#15b01a',
    )

    noises = [
        noise.newtonian.Rayleigh,
        noise.newtonian.Body,
        noise.newtonian.Infrasound,
    ]


class Coating(nb.Budget):
    """Coating Thermal

    """

    name = 'Coating'

    style = dict(
        label='Coating Thermal',
        color='#fe0002',
    )

    noises = [
        noise.coatingthermal.CoatingBrownian,
        noise.coatingthermal.CoatingThermoOptic,
    ]


class Substrate(nb.Budget):
    """Substrate Thermal

    """

    name = 'Substrate'

    style = dict(
        label='Substrate Thermal',
        color='#fb7d07',
    )

    noises = [
        noise.substratethermal.ITMThermoRefractive,
        noise.substratethermal.SubstrateBrownian,
        noise.substratethermal.SubstrateThermoElastic,
    ]


class CE2silicon(nb.Budget):

    name = 'Cosmic Explorer 2 (Silicon)'

    noises = [
        Quantum,
        noise.seismic.Seismic,
        Newtonian,
        noise.suspensionthermal.SuspensionThermal,
        Coating,
        Substrate,
        noise.residualgas.ResidualGas,
    ]

    calibrations = [
        Strain,
    ]

    plot_style = PLOT_STYLE


class TotalRelGamma(nb.Budget):
    """
    Total noise relative to unsqueezed vacuum
    Includes the optomechanical gain.
    """

    name = 'Cosmic Explorer 2 (Silicon)'

    plot_style = dict(
        psd=True,
        dbs=True,
        ylim_limits=(1e-2, 2),
        legend_kw=dict(fontsize='x-small', ncol=3),
        ylabel="Noise relative to unsqueezed vacuum",
        ylabel2="Effective Loss Level",
    )

    noises = [
        Quantum,
        noise.seismic.Seismic,
        Newtonian,
        noise.suspensionthermal.SuspensionThermal,
        Coating,
        Substrate,
        noise.residualgas.ResidualGas,
    ]

    calibrations = [
        QuantumDisplacementInvCal,
        QuantumASPortInvCal,
    ]
